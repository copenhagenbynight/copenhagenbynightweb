﻿using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseLibrary;
using PostSharp.Patterns.Contracts;

namespace AccountRolesLibrary.Encapsulated.Repositories
{
    internal class RoleRepository
    {
        readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();
        internal List<AspNetRoles> Get()
        {
            return _copenhagenByNightEntities.AspNetRoles.Select(item => item).ToList();
        }

        internal List<AspNetRoles> Get([NotEmpty] Guid value)
        {
            return _copenhagenByNightEntities.AspNetRoles.Where(item => item.Id == value.ToString()).Select(item => item).ToList();
        }

        internal List<AspNetRoles> Get([NotNull] string value)
        {
            return _copenhagenByNightEntities.AspNetRoles.Where(item => item.Name.Contains(value)).Select(item => item).ToList();
        }

        internal void Add([NotNull] AspNetRoles model)
        {
            _copenhagenByNightEntities.AspNetRoles.Add(model);
        }


        internal void Update([NotEmpty] Guid value, [NotNull] AspNetRoles model)
        {
            AspNetRoles currentAspNetRole =
                _copenhagenByNightEntities.AspNetRoles.Where(item => item.Id == model.Id)
                    .Select(item => item).FirstOrDefault();
            // ReSharper disable once PossibleNullReferenceException
            currentAspNetRole.Name = model.Name;
            _copenhagenByNightEntities.SaveChanges();
        }

        internal void Delete([NotEmpty] Guid value)
        {
            var aspNetRoles = new AspNetRoles { Id = value.ToString() };
            _copenhagenByNightEntities.AspNetRoles.Attach(aspNetRoles);
            _copenhagenByNightEntities.AspNetRoles.Remove(aspNetRoles);
            _copenhagenByNightEntities.SaveChanges();
        }
    }
}
