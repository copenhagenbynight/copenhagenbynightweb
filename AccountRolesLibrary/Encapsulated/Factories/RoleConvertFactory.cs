﻿using System;
using System.Collections.Generic;
using System.Linq;
using AccountRolesLibrary.Exposed.Models;
using DatabaseLibrary;
using PostSharp.Patterns.Contracts;

namespace AccountRolesLibrary.Encapsulated.Factories
{
    internal static class RoleConvertFactory
    {

        internal static RoleModel Convert([NotNull]AspNetRoles item)
        {
            return new RoleModel
            {
               RoleId = Guid.Parse(item.Id),
               Name = item.Name
                
            };
        }

        internal static List<RoleModel> Convert([NotNull] List<AspNetRoles> list)
        {
            return list.Select(Convert).ToList();
        }

        internal static AspNetRoles Convert(RoleAddModel model)
        {
           return new AspNetRoles
           {
               Name = model.Name
           };
        }

        internal static AspNetRoles Convert(RoleUpdateModel model)
        {
            return new AspNetRoles
            {
                Id = model.RoleId.ToString(), 
                Name = model.Name
            };
        }
    }
}
