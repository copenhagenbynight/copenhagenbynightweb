﻿using System;
using System.Collections.Generic;
using System.Linq;
using AccountRolesLibrary.Encapsulated.Factories;
using AccountRolesLibrary.Encapsulated.Repositories;
using AccountRolesLibrary.Exposed.Models;

namespace AccountRolesLibrary.Encapsulated.Managers
{
    internal class RoleManager
    {
        readonly RoleRepository _roleRepository = new RoleRepository();

        internal List<RoleModel> Get()
        {
           return  RoleConvertFactory.Convert(_roleRepository.Get());
        }

        internal List<RoleModel> GetById(Guid value)
        {
            return RoleConvertFactory.Convert(_roleRepository.Get(value));
        }

        internal List<RoleModel> SearchByName(string value)
        {
            return RoleConvertFactory.Convert(_roleRepository.Get(value));
        }

        internal void Delete(Guid value)
        {
            var firstOrDefault = GetById(value).FirstOrDefault();
            if (firstOrDefault != null && firstOrDefault.Name == "Administrator")
                throw new AccessViolationException();
            
            _roleRepository.Delete(value);
        }
    }
}
