﻿using System.Collections.Generic;
using AccountRolesLibrary.Encapsulated.Managers;
using AccountRolesLibrary.Exposed.Models;
using PostSharp.Patterns.Contracts;

namespace AccountRolesLibrary.Exposed.Publishers
{
    public class RolePublisherWithSearch
    {
        private readonly RoleManager _roleManager = new RoleManager();

        public List<RoleModel> Get([NotNull] string value)
        {
            return _roleManager.SearchByName(value);
        }
    }
}
