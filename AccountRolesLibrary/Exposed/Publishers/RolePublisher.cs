﻿using System;
using System.Collections.Generic;
using AccountRolesLibrary.Encapsulated.Factories;
using AccountRolesLibrary.Encapsulated.Managers;
using AccountRolesLibrary.Encapsulated.Repositories;
using AccountRolesLibrary.Exposed.Models;
using PostSharp.Patterns.Contracts;

namespace AccountRolesLibrary.Exposed.Publishers
{
    public class RolePublisher
    {
        readonly RoleRepository _roleRepository = new RoleRepository();
        readonly RoleManager _roleManager = new RoleManager();
        public List<RoleModel> Get()
        {
            return _roleManager.Get();
        }

        public List<RoleModel> Get([NotEmpty] Guid value)
        {
            return _roleManager.GetById(value);
        }

        public void Add([NotNull] RoleAddModel model)
        {
            _roleRepository.Add(RoleConvertFactory.Convert(model));
        }

        public void Update([NotEmpty] Guid value, [NotNull] RoleUpdateModel model)
        {
            _roleRepository.Update(value, RoleConvertFactory.Convert(model));
        }

        public void Delete([NotEmpty] Guid value)
        {
            _roleManager.Delete(value);
        }
    }
}
