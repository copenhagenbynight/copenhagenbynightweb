﻿using System;

namespace AccountRolesLibrary.Exposed.Models
{
    public class RoleModel
    {
        public string Name { get; internal set; }
        public Guid RoleId { get; internal set; }
    }
}