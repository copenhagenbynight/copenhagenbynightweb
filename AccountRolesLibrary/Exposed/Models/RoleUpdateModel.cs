﻿using System;

namespace AccountRolesLibrary.Exposed.Models
{
    public class RoleUpdateModel
    {
        public Guid RoleId { internal get;  set; }
        public string Name { internal get;  set; }
        
    }
}