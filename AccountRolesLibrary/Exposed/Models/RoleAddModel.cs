﻿namespace AccountRolesLibrary.Exposed.Models
{
    public class RoleAddModel
    {
        public string Name { internal get;  set; }
    }
}