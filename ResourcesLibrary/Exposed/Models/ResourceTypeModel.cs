﻿using System;

namespace ResourcesLibrary.Exposed.Models
{
    public class ResourceTypeModel
    {
        public Guid ResourceTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}