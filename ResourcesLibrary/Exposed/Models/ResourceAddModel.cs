﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResourcesLibrary.Exposed.Models
{
    public class ResourceAddModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid ResourceTypeId { get; set; }
    }
}
