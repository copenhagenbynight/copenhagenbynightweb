﻿using System;

namespace ResourcesLibrary.Exposed.Models
{
    public class ResourceModel
    {
        public Guid ResourceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ResourceTypeModel ResourceType { get; set; }
    }
}