﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResourcesLibrary.Encapsulated.Managers;
using ResourcesLibrary.Exposed.Models;

namespace ResourcesLibrary.Exposed

{
    public class ResourcePublisher
    {
        private readonly ResourceManager _resourceManager = new ResourceManager();
        public ResourceModel Add(ResourceAddModel model)
        {
            return this._resourceManager.Add(model);
        }


        public void Delete(Guid resourceId)
        {
            this._resourceManager.Delete(resourceId);
        }
    }
}

