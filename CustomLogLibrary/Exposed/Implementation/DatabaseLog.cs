﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomLogLibrary.Exposed.Implementation
{
    using System.Runtime.Remoting.Messaging;
    using PostSharp.Aspects;
    using PostSharp.Serialization;

    [PSerializable]
    public class DatabaseLog : ICustomLog
    {
        public void OnEntry(string methodName, MethodExecutionArgs args)
        {
            this.Log(LogEventType.OnEntry, args);
        }

        public void OnException(string methodName, MethodExecutionArgs args)
        {
            this.Log(LogEventType.OnException, args);
        }

        public void OnSuccess(string methodName, MethodExecutionArgs args)
        {
            this.Log(LogEventType.OnSuccess, args);
        }

        public void OnExit(string methodName, MethodExecutionArgs args)
        {
            this.Log(LogEventType.OnExit, args);
        }

        internal void Log(LogEventType logEvent,MethodExecutionArgs args)
        {
            if (!this.NeedLog(logEvent,args))
                return;



        }

        private bool NeedLog(LogEventType logEvent, MethodExecutionArgs args)
        {
            return true;
        }
    }

    internal enum LogEventType

    {
        OnEntry,
        OnExit,
        OnException,
        OnSuccess
    }
}
