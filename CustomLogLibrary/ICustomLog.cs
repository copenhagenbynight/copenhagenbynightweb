﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomLogLibrary
{
    using PostSharp.Aspects;

    public interface ICustomLog
    {
        void OnEntry(string methodName, MethodExecutionArgs args);
        void OnExit(string methodName, MethodExecutionArgs args);
        void OnException(string methodName, MethodExecutionArgs args);
        void OnSuccess(string methodName, MethodExecutionArgs args);
    }
}
