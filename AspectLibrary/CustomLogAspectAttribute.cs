﻿namespace AspectLibrary
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;
    using System.Text;
    using PostSharp.Aspects;
    using PostSharp.Serialization;

    [PSerializable]
    public class LogLocationInfomation
    {
        public string Assembly { get; set; }
        public string Type { get; set; }
        public string Class { get; set; }
        public string Method { get; set; }
        public string User { get; set; }
    }

    enum OnLogLocationInfomation
    {
        OnEntry,
        OnSuccess,
        OnExit,
        OnException

    }

    [PSerializable]
    public class CustomLogAspectAttribute : OnMethodBoundaryAspect
    {
        [NonSerialized]
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private LogLocationInfomation _logLocationInfomation = new LogLocationInfomation();

        public override void CompileTimeInitialize(MethodBase method, AspectInfo aspectInfo)
        {
            // ReSharper disable PossibleNullReferenceException
            string[] asArray = method.DeclaringType.Namespace.Split('.');
            // ReSharper restore PossibleNullReferenceException
            this._logLocationInfomation.Assembly = asArray[0];
            this._logLocationInfomation.Type = asArray[asArray.Length -1];
            this._logLocationInfomation.Class = method.DeclaringType.Name;
            this._logLocationInfomation.Method = method.Name;
        }


        public override void OnEntry(MethodExecutionArgs args)
        {
            CustomLog(this._logLocationInfomation, OnLogLocationInfomation.OnEntry, args);
        }



        public override void OnExit(MethodExecutionArgs args)
        {
            CustomLog(this._logLocationInfomation, OnLogLocationInfomation.OnEntry, args);
        }

        public override void OnException(MethodExecutionArgs args)
        {
            CustomLog(this._logLocationInfomation, OnLogLocationInfomation.OnException, args);
        }

        public override void OnSuccess(MethodExecutionArgs args)
        {
            CustomLog(this._logLocationInfomation, OnLogLocationInfomation.OnSuccess, args);
        }

        private static void CustomLog(LogLocationInfomation logLocationInfomation, OnLogLocationInfomation boundary, MethodExecutionArgs args)
        {
            
            logLocationInfomation.User = GetCurrentUser();

            if (!NeedLog(logLocationInfomation))
                return;

            StringBuilder currentLog = new StringBuilder("[" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt") + "] - " + boundary + ":");

            currentLog = BaseInfomation(logLocationInfomation,currentLog);

            if (OnLogLocationInfomation.OnEntry == boundary)
                currentLog = StringifyArgument(currentLog, args);

            if (OnLogLocationInfomation.OnExit == boundary && OnLogLocationInfomation.OnSuccess == boundary)
                currentLog = StringifyReturnValue(currentLog, args);

            if (OnLogLocationInfomation.OnException == boundary)
                currentLog = StringifyException(currentLog, args);

            Console.WriteLine(currentLog);
        }

        private static bool NeedLog(LogLocationInfomation logLocationInfomation)
        {


            Dictionary<string, bool> needLog = GetInfomationAboutWhenToLog();

            if (needLog.Count == 0)
                return false;

            if (needLog.ContainsKey("User#" + logLocationInfomation.User)
                 || needLog.ContainsKey("Assembly#" + logLocationInfomation.Assembly)
                 || needLog.ContainsKey("Type#" + logLocationInfomation.Type)
                 || needLog.ContainsKey("Class#" + logLocationInfomation.Class)
                 || needLog.ContainsKey("Method#" + logLocationInfomation.Method )
                 || needLog.ContainsKey("Assembly#" + logLocationInfomation.Assembly + "Type#" + logLocationInfomation.Type +"Method#" + logLocationInfomation.Method)
                 || needLog.ContainsKey("Assembly#" + logLocationInfomation.Assembly + "Type#" + logLocationInfomation.Type)
                 || needLog.ContainsKey("Assembly#" + logLocationInfomation.Assembly + "Method#" + logLocationInfomation.Method)
                 || needLog.ContainsKey( "Type#" + logLocationInfomation.Type + "Method#" + logLocationInfomation.Method)
                 || needLog.ContainsKey("Class#" + logLocationInfomation.Assembly + "Type#" + logLocationInfomation.Type + "Method#" + logLocationInfomation.Method)
                 || needLog.ContainsKey("Class#" + logLocationInfomation.Assembly + "Method#" + logLocationInfomation.Method)
                 || needLog.ContainsKey("User#" + logLocationInfomation.User + "Type#" + logLocationInfomation.Type)
                 || needLog.ContainsKey("User#" + logLocationInfomation.User + "Class#" + logLocationInfomation.Class)
                 || needLog.ContainsKey("User#" + logLocationInfomation.User + "Method#" + logLocationInfomation.Method)
                 || needLog.ContainsKey("User#" + logLocationInfomation.User + "Assembly#" + logLocationInfomation.Assembly + "Type#" + logLocationInfomation.Type + "Method#" + logLocationInfomation.Method)
                 || needLog.ContainsKey("User#" + logLocationInfomation.User + "Assembly#" + logLocationInfomation.Assembly + "Type#" + logLocationInfomation.Type)
                 || needLog.ContainsKey("User#" + logLocationInfomation.User + "Assembly#" + logLocationInfomation.Assembly + "Method#" + logLocationInfomation.Method)
                 || needLog.ContainsKey("User#" + logLocationInfomation.User + "Type#" + logLocationInfomation.Type + "Method#" + logLocationInfomation.Method)
                 || needLog.ContainsKey("User#" + logLocationInfomation.User + "Class#" + logLocationInfomation.Assembly + "Type#" + logLocationInfomation.Type + "Method#" + logLocationInfomation.Method)
                 || needLog.ContainsKey("User#" + logLocationInfomation.User + "Class#" + logLocationInfomation.Assembly + "Method#" + logLocationInfomation.Method))
                return true;

            return false;
        }

        private static Dictionary<string, bool> GetInfomationAboutWhenToLog()
        {
            return new Dictionary<string, bool>();
        }

        private static StringBuilder BaseInfomation(LogLocationInfomation logLocationInfomation , StringBuilder currentLog)
        {
            currentLog.Append("User: " + GetCurrentUser());
            currentLog.Append("Assembly: " + logLocationInfomation.Assembly);
            currentLog.Append(" ");
            currentLog.Append("Type: " + logLocationInfomation.Type);
            currentLog.Append(" ");
            currentLog.Append("Class: " + logLocationInfomation.Class);
            currentLog.Append(" ");
            currentLog.Append("Method: " + logLocationInfomation.Method);
            return currentLog;
        }

        private static StringBuilder StringifyException(StringBuilder currentLog, MethodExecutionArgs args)
        {
            currentLog.Append("[StackTrace]: ");
            StackTrace st = new StackTrace(args.Exception, true);


            for (int i = 0; i < st.FrameCount; i++)

            {
                StackFrame sf = st.GetFrame(i);
                currentLog.Append("Frame: ["+ i + "] File: " + sf.GetFileName() + " Method: " + sf.GetMethod().Name + " Line: " +
                   sf.GetFileLineNumber() + "StackTrace: " + args.Exception);
            }
            return currentLog;
        }

        private static StringBuilder StringifyReturnValue(StringBuilder currentLog, MethodExecutionArgs args)
        {
            currentLog.Append("ReturnValue (" + args.ReturnValue.GetType() + "): " + args.ReturnValue);
            return currentLog;
        }

        private static StringBuilder StringifyArgument(StringBuilder currentLog, MethodExecutionArgs args)
        {
            currentLog.Append("Arguments: ");
            for (int i = 0; i < args.Arguments.Count; i++)
            {
                object current = args.Arguments.GetArgument(i);

                currentLog.Append("\n\t");
                currentLog.Append(" Name: " + args.Method.GetParameters()[i].Name);
                currentLog.Append(" (" + current.GetType() + ")");
                currentLog.Append(" Value:" + current);
            }
            return currentLog;
        }

        private static string GetCurrentUser()
        {
            return "8a9e3359-eaf7-4d8c-a975-94836593d913";
        }
    }
}