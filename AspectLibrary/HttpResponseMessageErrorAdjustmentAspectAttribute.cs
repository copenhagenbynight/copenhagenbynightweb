﻿namespace AspectLibrary
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Http;
    using System.Reflection;
    using System.Text;
    using PostSharp.Aspects;

    [Serializable]
    public sealed class HttpResponseMessageErrorAdjustmentAspectAttribute : OnExceptionAspect
    {
        [NonSerialized] private readonly Type _exceptionType;

        public HttpResponseMessageErrorAdjustmentAspectAttribute() : this(null)
        {
        }

        public HttpResponseMessageErrorAdjustmentAspectAttribute(Type exceptionType)
        {
            this._exceptionType = exceptionType;
        }

        public string Message { get; set; } = "{0}";

        public override Type GetExceptionType(MethodBase method)
        {
            return this._exceptionType;
        }

        public override void OnException(MethodExecutionArgs args)
        {
            
            args.ReturnValue = new HttpResponseMessage
            {
                Content = this.GetErrorMessage(args.Exception),
                StatusCode = this.GetHttpStatusCode(args.Exception)
            };

            args.FlowBehavior = FlowBehavior.Return;
        }

        private HttpStatusCode GetHttpStatusCode(Exception exception)
        {
            if (exception.Data.Contains("httpStatusCode"))
                return (HttpStatusCode) exception.Data["httpStatusCode"];

            return HttpStatusCode.BadRequest;
        }

        private StringContent GetErrorMessage(Exception exception)
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["StackTraceToWeb"]))
                return new StringContent(GetStackTrace(exception));

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["TranslatableErrorMessageToWeb"]))
                return new StringContent(GetTranslatableErrorMessage(exception));

            return new StringContent(exception.Message);
        }

        private static string GetTranslatableErrorMessage(Exception exception)
        {
            return
                exception.Data.Contains("TranslatableErrorMessage")
                    ? (string) exception.Data["TranslatableErrorMessage"]
                    : exception.Message;
        }

        private static string GetStackTrace(Exception exception)
        {
            StringBuilder stringBuilder = new StringBuilder("[StackTrace]: ");
            StackTrace st = new StackTrace(exception, true);


            for (int i = 0; i < st.FrameCount; i++)
               
            {
                StackFrame sf = st.GetFrame(i);
                stringBuilder.Append("File: " + sf.GetFileName() + " Method: " + sf.GetMethod().Name + " Line: " +
                   sf.GetFileLineNumber() + "StackTrace: " + exception);
            }
            return stringBuilder.ToString();
        }
    }
}
