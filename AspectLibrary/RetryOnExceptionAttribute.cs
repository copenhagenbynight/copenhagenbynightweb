﻿namespace AspectLibrary
{
    using System;
    using PostSharp.Aspects;

    [Serializable]
    public class RetryOnExceptionAttribute : MethodInterceptionAspect
    {
        public int MaxRetries { get; set; }

        public override void OnInvoke(MethodInterceptionArgs args)
        {
            int retriesCounter = 0;

            while (true)
            {
                try
                {
                    base.OnInvoke(args);
                    return;
                }
                catch (Exception)
                {
                    retriesCounter++;
                    if (retriesCounter > this.MaxRetries) throw;
                }
            }
        }
    }
}