﻿namespace AspectLibrary
{
    using System;
    using PostSharp.Aspects;
    using PostSharp.Patterns.Contracts;
    using PostSharp.Reflection;
    using PostSharp.Serialization;

    public class GuidNotNullAttribute : LocationContractAttribute, ILocationValidationAspect<Guid>
    {
        public new const string ErrorMessage = "GuidNotNull";

        protected override string GetErrorMessage()
        {
            return "Value Guid {2} must have a non-00000000-0000-0000-0000-000000000000 value.";
        }


        public Exception ValidateValue(Guid value, string name, LocationKind locationKind)
        {
            if (value == Guid.Empty)
                return this.CreateArgumentNullException(value, name, locationKind);
            
            return null;
        }


    }
}
