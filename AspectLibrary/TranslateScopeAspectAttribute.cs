﻿namespace AspectLibrary
{
    using System;
    using System.Transactions;
    using PostSharp.Aspects;

    [Serializable]
    public class TranslateScopeAspectAttribute : MethodInterceptionAspect
    {
        public int MaxRetries { get; set; }

        public override void OnInvoke(MethodInterceptionArgs args)
        {
            int retriesCounter = 0;

            while (true)
                using (TransactionScope scope = new TransactionScope())
                {
                    try
                    {
                        args.Proceed();
                        scope.Complete();
                        return;
                    }
                    catch (Exception)
                    {
                        retriesCounter++;
                        if (retriesCounter > this.MaxRetries) throw;
                    }
                }
        }
    }
}