﻿namespace AspectLibrary
{
    using System;
    using System.ComponentModel;
    using System.Web;
    using PostSharp.Aspects;

    [Serializable]
    public class CacheAspectAttribute : MethodInterceptionAspect
    {
        [NonSerialized]
        private readonly CacheAspectExpirationType _cacheAspectExpirationType;
        private readonly long _cacheAspectExpirationMinutes;

        public CacheAspectAttribute()
        {
            this._cacheAspectExpirationType = CacheAspectExpirationType.None;
        }

        public CacheAspectAttribute(CacheAspectExpirationType cacheAspectExpirationType, long minutes)
        {
            this._cacheAspectExpirationType = cacheAspectExpirationType;
            this._cacheAspectExpirationMinutes = minutes;
        }

        public override void OnInvoke(MethodInterceptionArgs methodInterceptionArgs)
        {
    
            CacheAspectAction canBeApplyedCacheAspect;

            if (!Enum.TryParse(methodInterceptionArgs.Method.Name, out canBeApplyedCacheAspect))
                throw new InvalidEnumArgumentException("CacheAspect used on method with wrong name!");

            // ReSharper disable once PossibleNullReferenceException
            string cacheKeyBase = methodInterceptionArgs.Method.DeclaringType.Name;
            string cacheKey = cacheKeyBase + "All";

            if ((methodInterceptionArgs.Arguments.Count != 0)
                && methodInterceptionArgs.Arguments[0] is Guid)
                cacheKey = methodInterceptionArgs.Method.DeclaringType.Name + methodInterceptionArgs.Arguments[0];


            if ((methodInterceptionArgs.Method.Name == CacheAspectAction.Get.ToString()) &&
                (HttpRuntime.Cache[cacheKey] != null))
            {
                methodInterceptionArgs.ReturnValue = HttpRuntime.Cache[cacheKey];
                return;
            }

            ExpirationInformation expirationInformation = this.GetExpirationInformation();

            object returnVal = methodInterceptionArgs.Invoke(methodInterceptionArgs.Arguments);

            ClanCache(cacheKeyBase, cacheKey);

            if (returnVal != null)
                HttpRuntime.Cache.Insert(cacheKey, returnVal, null, expirationInformation.AbsoluteExpiration, expirationInformation.SlidingExpiration);

            methodInterceptionArgs.ReturnValue = returnVal;
        }

        private ExpirationInformation GetExpirationInformation()
        {
            ExpirationInformation expirationInformation = new ExpirationInformation();
            switch (this._cacheAspectExpirationType)
            {
                case CacheAspectExpirationType.Absolute:
                    expirationInformation.AbsoluteExpiration = DateTime.Now.AddMinutes(this._cacheAspectExpirationMinutes);
                    break;
                case CacheAspectExpirationType.Sliding:
                    expirationInformation.SlidingExpiration = TimeSpan.FromMinutes(this._cacheAspectExpirationMinutes);
                    break;
            }

            return expirationInformation;
        }

        private static void ClanCache(string cacheKeyBase, string cacheKey)
        {
            if (HttpRuntime.Cache[cacheKey] != null)
                HttpRuntime.Cache.Remove(cacheKey);

            if (HttpRuntime.Cache[cacheKeyBase + "All"] != null)
                HttpRuntime.Cache.Remove(cacheKeyBase + "All");
        }
    }

    public enum CacheAspectAction
    {
        Get,
        Add,
        Update,
        Delete,
        SetDefault
    }

    public enum CacheAspectExpirationType
    {
        None,
        Absolute,
        Sliding
    }

    public class ExpirationInformation
    {
        public DateTime AbsoluteExpiration { get; set; } = DateTime.MaxValue;
        public TimeSpan SlidingExpiration { get; set; } = TimeSpan.Zero;
    }
}
