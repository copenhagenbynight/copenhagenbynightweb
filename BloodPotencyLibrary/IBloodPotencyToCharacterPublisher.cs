﻿namespace BloodPotencyLibrary
{
    using System;
    using AspectLibrary;
    using Exposed.Models;

    public interface IBloodPotencyToCharacterPublisher
    {
        BloodPotencyModel Get();
        BloodPotencyModel Update([GuidNotNull] Guid bloodPotencyId);
        BloodPotencyModel SetDefault();
    }
}