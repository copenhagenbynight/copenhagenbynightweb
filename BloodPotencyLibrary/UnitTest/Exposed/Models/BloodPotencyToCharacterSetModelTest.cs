﻿namespace BloodPotencyLibrary.Exposed.Models
{
    using System;
    using System.Collections.Generic;
    using CustomAssertLibrary;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class BloodPotencyToCharacterSetModelTest
    {
        [TestMethod]
        public void BloodPotencyAddModelTestMethod()
        {
            Dictionary<string, Type> listOfTypeValidations = new Dictionary<string, Type>
            {
                {"BloodPotencyId", typeof(Guid)}
            };

            CustomAssert.PropertyIsCorrect(typeof(BloodPotencyToCharacterSetModel), listOfTypeValidations);
        }
    }
}
