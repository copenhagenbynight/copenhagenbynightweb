﻿namespace BloodPotencyLibrary.Exposed.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CustomAssertLibrary;
    using Encapsulated.Managers;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class BloodPotencyModelTest
    {
        [TestMethod]
        public void BloodPotencyAddModelTestMethod()
        {
            Dictionary<string, Type> listOfTypeValidations = new Dictionary<string, Type>
            {
                {"BloodPotencyId", typeof(Guid)},
                {"BaseAttackBonusMax", typeof(int)},
                {"BloodPotencyLevel", typeof(int)},
                {"SustenanceId", typeof(Guid)},
                {"MentalResistancePoints", typeof(int)},
                {"DisciplinesPrCombat", typeof(int)},
                {"BonusArmor", typeof(int)}
            };

            CustomAssert.PropertyIsCorrect(typeof(BloodPotencyModel), listOfTypeValidations);
        }

        [TestMethod] //StrictlyPositive
        public void BaseAttackBonusMaxTestMethod()
        {
            BloodPotencyUpdateModel bloodPotencyAddModel = new BloodPotencyUpdateModel();

            CustomAssert.Throws<ArgumentOutOfRangeException>(() => bloodPotencyAddModel.BaseAttackBonusMax = -1);
            CustomAssert.Throws<ArgumentOutOfRangeException>(() => bloodPotencyAddModel.BaseAttackBonusMax = 0);

            bloodPotencyAddModel.BaseAttackBonusMax = 1;
            bloodPotencyAddModel.BaseAttackBonusMax = int.MaxValue;
        }

        [TestMethod] //Range(1,10))
        public void BloodPotencyLevelTestMethod()
        {
            BloodPotencyUpdateModel bloodPotencyAddModel = new BloodPotencyUpdateModel();

            CustomAssert.Throws<ArgumentOutOfRangeException>(() => bloodPotencyAddModel.BloodPotencyLevel = -1);
            CustomAssert.Throws<ArgumentOutOfRangeException>(() => bloodPotencyAddModel.BloodPotencyLevel = 0);
            CustomAssert.Throws<ArgumentOutOfRangeException>(() => bloodPotencyAddModel.BloodPotencyLevel = 11);

            bloodPotencyAddModel.BloodPotencyLevel = 1;
            bloodPotencyAddModel.BloodPotencyLevel = 10;
        }

        [TestMethod] //StrictlyPositive
        public void MentalResistancePointsTestMethod()
        {
            BloodPotencyUpdateModel bloodPotencyAddModel = new BloodPotencyUpdateModel();

            CustomAssert.Throws<ArgumentOutOfRangeException>(() => bloodPotencyAddModel.MentalResistancePoints = -1);
            CustomAssert.Throws<ArgumentOutOfRangeException>(() => bloodPotencyAddModel.MentalResistancePoints = 0);

            bloodPotencyAddModel.MentalResistancePoints = 1;
            bloodPotencyAddModel.MentalResistancePoints = int.MaxValue;
        }

        [TestMethod] //StrictlyPositive
        public void DisciplinesPrCombatTestMethod()
        {
            BloodPotencyUpdateModel bloodPotencyAddModel = new BloodPotencyUpdateModel();

            CustomAssert.Throws<ArgumentOutOfRangeException>(() => bloodPotencyAddModel.DisciplinesPrCombat = -1);
            CustomAssert.Throws<ArgumentOutOfRangeException>(() => bloodPotencyAddModel.DisciplinesPrCombat = 0);

            bloodPotencyAddModel.DisciplinesPrCombat = 1;
            bloodPotencyAddModel.DisciplinesPrCombat = int.MaxValue;
        }

        [TestMethod] //StrictlyPositive
        public void BonusArmorTestMethod()
        {
            BloodPotencyUpdateModel bloodPotencyAddModel = new BloodPotencyUpdateModel();

            CustomAssert.Throws<ArgumentOutOfRangeException>(() => bloodPotencyAddModel.BonusArmor = -1);
            
            bloodPotencyAddModel.BonusArmor = 0;
            bloodPotencyAddModel.BonusArmor = int.MaxValue;
        }
    }
}
