﻿namespace BloodPotencyLibrary.Exposed.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Models;

    [TestClass]
    public class BloodPotencyPublisherTest
    {
        private readonly BloodPotencyPublisher _publisher = new BloodPotencyPublisher();

        [TestMethod]
        public void GetTestMethod()
        {
            List<BloodPotencyModel> getResult = this._publisher.Get();
            Assert.AreEqual(getResult.Count, 10);
        }

        [TestMethod]
        public void GetByBloodPotencyIdTestMethod()
        {
            BloodPotencyModel getOneForCompare = this._publisher.Get().FirstOrDefault();
            // ReSharper disable once PossibleNullReferenceException
            BloodPotencyModel getResult = this._publisher.Get(getOneForCompare.BloodPotencyId);

            Assert.AreEqual(getOneForCompare.BloodPotencyLevel, getResult.BloodPotencyLevel);
            Assert.AreEqual(getOneForCompare.BaseAttackBonusMax, getResult.BaseAttackBonusMax);
            Assert.AreEqual(getOneForCompare.BonusArmor, getResult.BonusArmor);
            Assert.AreEqual(getOneForCompare.DisciplinesPrCombat, getResult.DisciplinesPrCombat);
            Assert.AreEqual(getOneForCompare.MentalResistancePoints, getResult.MentalResistancePoints);
            Assert.AreEqual(getOneForCompare.SustenanceId, getResult.SustenanceId);
        }

        [TestMethod]
        public void AddUpdateDeleteTestMethod()
        {
            BloodPotencyAddModel newBloodPotencyAddModel = new BloodPotencyAddModel
            {
                MentalResistancePoints = 1,
                BaseAttackBonusMax = 1,
                BloodPotencyLevel = 1,
                BonusArmor = 1,
                DisciplinesPrCombat = 1,
                SustenanceId = Guid.Parse("7F015C52-58D8-434F-BFD9-69F032B3F3C7")
            };


            BloodPotencyModel resultAdd = this._publisher.Add(newBloodPotencyAddModel);

            BloodPotencyUpdateModel newBloodPotencyUpdateModel = new BloodPotencyUpdateModel
            {
                BloodPotencyId = resultAdd.BloodPotencyId,
                MentalResistancePoints = 2,
                BaseAttackBonusMax = 2,
                BloodPotencyLevel = 2,
                BonusArmor = 2,
                DisciplinesPrCombat = 2,
                SustenanceId = Guid.Parse("7F015C52-58D8-434F-BFD9-69F032B3F3C7")
            };
            BloodPotencyModel resultUpdate = this._publisher.Update(resultAdd.BloodPotencyId,newBloodPotencyUpdateModel);


            // ReSharper disable once PossibleNullReferenceException
            Assert.AreEqual(resultAdd.BloodPotencyId, resultUpdate.BloodPotencyId );
            Assert.AreEqual(resultAdd.BloodPotencyLevel + 1, resultUpdate.BloodPotencyLevel);
            Assert.AreEqual(resultAdd.BaseAttackBonusMax + 1, resultUpdate.BaseAttackBonusMax);
            Assert.AreEqual(resultAdd.BonusArmor + 1, resultUpdate.BonusArmor);
            Assert.AreEqual(resultAdd.DisciplinesPrCombat + 1, resultUpdate.DisciplinesPrCombat);
            Assert.AreEqual(resultAdd.MentalResistancePoints + 1, resultUpdate.MentalResistancePoints);
            Assert.AreEqual(resultAdd.SustenanceId, resultUpdate.SustenanceId);

           Assert.IsTrue(this._publisher.Delete(resultAdd.BloodPotencyId));
        }
    }
}
