﻿namespace BloodPotencyLibrary.Encapsulated.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseLibrary;
    using Exposed;
    using Exposed.Implementation;
    using Exposed.Models;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class BloodPotencyToCharacterPublisherTest
    {


        [TestMethod]
        public void GetTestMethod()
        {
            BloodPotencyToCharacterPublisher publisher = new BloodPotencyToCharacterPublisher(Guid.Parse("6FC08E27-44ED-46C4-AD82-110772AB76F8"));

            BloodPotencyModel  current = publisher.Get();
        }

        [TestMethod]
        public void UpdateTestMethod()
        {
            BloodPotencyToCharacterPublisher publisher = new BloodPotencyToCharacterPublisher(Guid.Parse("6FC08E27-44ED-46C4-AD82-110772AB76F8"));
            publisher.Update(Guid.Parse("9623445F-36A5-4855-B16F-F0DBAC6B72C6"));
            BloodPotencyModel current = publisher.Get();
            Assert.AreEqual(current.BloodPotencyId, Guid.Parse("9623445F-36A5-4855-B16F-F0DBAC6B72C6"));
            publisher.Update(Guid.Parse("3D21C40B-C236-413B-AE3D-94A450E16F78"));
        }
    }
}
