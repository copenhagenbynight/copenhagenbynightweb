﻿namespace BloodPotencyLibrary.Encapsulated.Managers
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class BloodPotencyToCharacterManagerTest
    {
        private readonly BloodPotencyToCharacterManager _manager = new BloodPotencyToCharacterManager();

        [TestMethod]
        public void SetDefaultAndGetTestMethod()
        {
            this._manager.SetDefault(Guid.Parse("6FC08E27-44ED-46C4-AD82-110772AB76F8"));
            Assert.AreEqual(this._manager.Get(Guid.Parse("6FC08E27-44ED-46C4-AD82-110772AB76F8")).BloodPotencyId, Guid.Parse("3D21C40B-C236-413B-AE3D-94A450E16F78"));
        }


        [TestMethod]
        public void UpdateTestMethod()
        {
            this._manager.Update(Guid.Parse("6FC08E27-44ED-46C4-AD82-110772AB76F8"), Guid.Parse("9623445F-36A5-4855-B16F-F0DBAC6B72C6"));

            Assert.AreEqual(this._manager.Get(Guid.Parse("6FC08E27-44ED-46C4-AD82-110772AB76F8")).BloodPotencyId, Guid.Parse("9623445F-36A5-4855-B16F-F0DBAC6B72C6"));
            this._manager.SetDefault(Guid.Parse("6FC08E27-44ED-46C4-AD82-110772AB76F8"));
        }



      
    }
}
