﻿namespace BloodPotencyLibrary.Encapsulated.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseLibrary;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class BloodPotencyToCharacterRepositoryTest
    {
        private readonly BloodPotencyToCharacterRepository _repository = new BloodPotencyToCharacterRepository();

        [TestMethod]
        public void GetTestMethod()
        {
            BloodPotency current = this._repository.Get(Guid.Parse("6FC08E27-44ED-46C4-AD82-110772AB76F8"));
        }

        [TestMethod]
        public void UpdateTestMethod()
        {
            
            this._repository.Update(Guid.Parse("6FC08E27-44ED-46C4-AD82-110772AB76F8"), Guid.Parse("9623445F-36A5-4855-B16F-F0DBAC6B72C6"));
            BloodPotency current = this._repository.Get(Guid.Parse("6FC08E27-44ED-46C4-AD82-110772AB76F8"));
            Assert.AreEqual(current.BloodPotencyId, Guid.Parse("9623445F-36A5-4855-B16F-F0DBAC6B72C6"));
            this._repository.Update(Guid.Parse("6FC08E27-44ED-46C4-AD82-110772AB76F8"), Guid.Parse("3D21C40B-C236-413B-AE3D-94A450E16F78"));
        }
    }
}
