﻿namespace BloodPotencyLibrary.Encapsulated.Repositories.TablesUsed
{
    using System;
    using System.Collections.Generic;

    using DatabaseLibrary;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using CustomAssertLibrary;

    [TestClass]
    public class TablesUsedBloodPotencyTest
    {
        [TestMethod]
        public void BloodPotencyDatalTestMethod()
        {
            Dictionary<string, Type> listOfTypeValidations = new Dictionary<string, Type>
            {
                {"BloodPotencyId", typeof(Guid)},
                {"BloodPotencyLevel", typeof(int)},
                { "BaseAttackBonusMax", typeof(int)},
                {"MentalResistancePoints", typeof(int)},
                {"DisciplinesPrCombat", typeof(int)},
                {"BonusArmor", typeof(int)},
                {"SustenanceId", typeof(Guid)},
                {"Sustenance", typeof(Sustenance)},
                {"Character", typeof(ICollection<Character>)}
            };

            CustomAssert.PropertyIsCorrect(typeof(BloodPotency), listOfTypeValidations);
        }
    }
}


