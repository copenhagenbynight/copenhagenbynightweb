﻿namespace BloodPotencyLibrary.Encapsulated.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseLibrary;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class BloodPotencyRepositoryTest
    {
        private readonly BloodPotencyRepository _repository = new BloodPotencyRepository();

        [TestMethod]
        public void GetTestMethod()
        {
            List<BloodPotency> getResult = this._repository.Get();
            Assert.AreEqual(getResult.Count, 10);
        }

        [TestMethod]
        public void GetByBloodPotencyIdTestMethod()
        {
            BloodPotency getOneForCompare = this._repository.Get().FirstOrDefault();
            // ReSharper disable once PossibleNullReferenceException
            BloodPotency getResult = this._repository.Get(getOneForCompare.BloodPotencyId);

            Assert.AreEqual(getOneForCompare.BloodPotencyLevel, getResult.BloodPotencyLevel);
            Assert.AreEqual(getOneForCompare.BaseAttackBonusMax, getResult.BaseAttackBonusMax);
            Assert.AreEqual(getOneForCompare.BonusArmor, getResult.BonusArmor);
            Assert.AreEqual(getOneForCompare.DisciplinesPrCombat, getResult.DisciplinesPrCombat);
            Assert.AreEqual(getOneForCompare.MentalResistancePoints, getResult.MentalResistancePoints);
            Assert.AreEqual(getOneForCompare.SustenanceId, getResult.SustenanceId);
        }

        [TestMethod]
        public void AddUpdateDeleteTestMethod()
        {
            BloodPotency newBloodPotencyAddModel = new BloodPotency
            {
                MentalResistancePoints = 1,
                BaseAttackBonusMax = 1,
                BloodPotencyLevel = 1,
                BonusArmor = 1,
                DisciplinesPrCombat = 1,
                SustenanceId = Guid.Parse("7F015C52-58D8-434F-BFD9-69F032B3F3C7")
            };


            BloodPotency resultAdd = this._repository.Add(newBloodPotencyAddModel);

            Assert.AreEqual(resultAdd.BloodPotencyId, newBloodPotencyAddModel.BloodPotencyId);
            Assert.AreEqual(1, newBloodPotencyAddModel.BloodPotencyLevel);
            Assert.AreEqual(1, newBloodPotencyAddModel.BaseAttackBonusMax);
            Assert.AreEqual(1, newBloodPotencyAddModel.BonusArmor);
            Assert.AreEqual(1, newBloodPotencyAddModel.DisciplinesPrCombat);
            Assert.AreEqual(1, newBloodPotencyAddModel.MentalResistancePoints);
            Assert.AreEqual(resultAdd.SustenanceId, newBloodPotencyAddModel.SustenanceId);
            BloodPotency newBloodPotencyUpdateModel = new BloodPotency
            {
                BloodPotencyId = resultAdd.BloodPotencyId,
                MentalResistancePoints = 2,
                BaseAttackBonusMax = 2,
                BloodPotencyLevel = 2,
                BonusArmor = 2,
                DisciplinesPrCombat = 2,
                SustenanceId = Guid.Parse("7F015C52-58D8-434F-BFD9-69F032B3F3C7")
            };
            BloodPotency resultUpdate = this._repository.Update(resultAdd.BloodPotencyId, newBloodPotencyUpdateModel);


            // ReSharper disable once PossibleNullReferenceException
            Assert.AreEqual(resultAdd.BloodPotencyId, resultUpdate.BloodPotencyId);
            Assert.AreEqual(2, resultUpdate.BloodPotencyLevel);
            Assert.AreEqual(2, resultUpdate.BaseAttackBonusMax);
            Assert.AreEqual(2, resultUpdate.BonusArmor);
            Assert.AreEqual(2, resultUpdate.DisciplinesPrCombat);
            Assert.AreEqual(2, resultUpdate.MentalResistancePoints);
            Assert.AreEqual(resultAdd.SustenanceId, resultUpdate.SustenanceId);

            Assert.IsTrue(this._repository.Delete(resultAdd.BloodPotencyId));
        }
    }
}
