﻿namespace BloodPotencyLibrary.Encapsulated.Factories
{
    using System;
    using DatabaseLibrary;
    using Exposed.Models;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class BloodPotencyFactoryTest
    {
        readonly BloodPotencyFactory _bloodPotencyFactory = new BloodPotencyFactory();
        [TestMethod]
        public void BloodPotencyToBloodPotencyModelTestMethod()
        {
            BloodPotency currentBase = new BloodPotency
            {
                BloodPotencyId = Guid.NewGuid(),
                BloodPotencyLevel = 1,
                BaseAttackBonusMax = 1,
                BonusArmor = 1,
                DisciplinesPrCombat = 1,
                MentalResistancePoints = 1,
                SustenanceId = Guid.NewGuid()
            };

            BloodPotencyModel converResult = this._bloodPotencyFactory.Convert(currentBase);

            Assert.AreEqual(currentBase.BloodPotencyId, converResult.BloodPotencyId);
            Assert.AreEqual(currentBase.BloodPotencyLevel, converResult.BloodPotencyLevel);
            Assert.AreEqual(currentBase.BaseAttackBonusMax, converResult.BaseAttackBonusMax);
            Assert.AreEqual(currentBase.BonusArmor, converResult.BonusArmor);
            Assert.AreEqual(currentBase.DisciplinesPrCombat, converResult.DisciplinesPrCombat);
            Assert.AreEqual(currentBase.MentalResistancePoints, converResult.MentalResistancePoints);
            Assert.AreEqual(currentBase.SustenanceId, converResult.SustenanceId);
        }

        [TestMethod]
        public void BloodPotencyModelToBloodPotencyTestMethod()
        {
            BloodPotencyModel currentBase = new BloodPotencyModel
            {
                BloodPotencyId = Guid.NewGuid(),
                BloodPotencyLevel = 1,
                BaseAttackBonusMax = 1,
                BonusArmor = 1,
                DisciplinesPrCombat = 1,
                MentalResistancePoints = 1,
                SustenanceId = Guid.NewGuid()
            };

            BloodPotency converResult = this._bloodPotencyFactory.Convert(currentBase);

            Assert.AreEqual(currentBase.BloodPotencyId, converResult.BloodPotencyId);
            Assert.AreEqual(currentBase.BloodPotencyLevel, converResult.BloodPotencyLevel);
            Assert.AreEqual(currentBase.BaseAttackBonusMax, converResult.BaseAttackBonusMax);
            Assert.AreEqual(currentBase.BonusArmor, converResult.BonusArmor);
            Assert.AreEqual(currentBase.DisciplinesPrCombat, converResult.DisciplinesPrCombat);
            Assert.AreEqual(currentBase.MentalResistancePoints, converResult.MentalResistancePoints);
            Assert.AreEqual(currentBase.SustenanceId, converResult.SustenanceId);
        }

        [TestMethod]
        public void BloodPotencyAddModelToBloodPotencyTestMethod()
        {
            BloodPotencyAddModel currentBase = new BloodPotencyAddModel
            {
                BloodPotencyLevel = 1,
                BaseAttackBonusMax = 1,
                BonusArmor = 1,
                DisciplinesPrCombat = 1,
                MentalResistancePoints = 1,
                SustenanceId = Guid.NewGuid()
            };

            BloodPotency converResult = this._bloodPotencyFactory.Convert(currentBase);

            Assert.AreEqual(currentBase.BloodPotencyLevel, converResult.BloodPotencyLevel);
            Assert.AreEqual(currentBase.BaseAttackBonusMax, converResult.BaseAttackBonusMax);
            Assert.AreEqual(currentBase.BonusArmor, converResult.BonusArmor);
            Assert.AreEqual(currentBase.DisciplinesPrCombat, converResult.DisciplinesPrCombat);
            Assert.AreEqual(currentBase.MentalResistancePoints, converResult.MentalResistancePoints);
            Assert.AreEqual(currentBase.SustenanceId, converResult.SustenanceId);
        }

        [TestMethod]
        public void BloodPotencyUpdateModelToBloodPotencyTestMethod()
        {
            BloodPotencyUpdateModel currentBase = new BloodPotencyUpdateModel
            {
                BloodPotencyLevel = 1,
                BaseAttackBonusMax = 1,
                BonusArmor = 1,
                DisciplinesPrCombat = 1,
                MentalResistancePoints = 1,
                SustenanceId = Guid.NewGuid()
            };

            BloodPotency converResult = this._bloodPotencyFactory.Convert(currentBase);

            Assert.AreEqual(currentBase.BloodPotencyId, converResult.BloodPotencyId);
            Assert.AreEqual(currentBase.BloodPotencyLevel, converResult.BloodPotencyLevel);
            Assert.AreEqual(currentBase.BaseAttackBonusMax, converResult.BaseAttackBonusMax);
            Assert.AreEqual(currentBase.BonusArmor, converResult.BonusArmor);
            Assert.AreEqual(currentBase.DisciplinesPrCombat, converResult.DisciplinesPrCombat);
            Assert.AreEqual(currentBase.MentalResistancePoints, converResult.MentalResistancePoints);
            Assert.AreEqual(currentBase.SustenanceId, converResult.SustenanceId);
        }


    }
}
