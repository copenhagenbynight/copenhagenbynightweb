﻿namespace BloodPotencyLibrary.Encapsulated.Managers
{
    using System;
    using System.Linq;
    using AspectLibrary;
    using Exposed.Models;
    using Factories;
    using Repositories;

    internal class BloodPotencyToCharacterManager
    {
        private readonly BloodPotencyRepository _bloodPotencyRepository = new BloodPotencyRepository();

        private readonly BloodPotencyToCharacterRepository _bloodPotencyToCharacterRepository =
            new BloodPotencyToCharacterRepository();

        private readonly BloodPotencyFactory _bloodPotencyFactory = new BloodPotencyFactory();

        [CustomLogAspect]
        internal BloodPotencyModel Update([GuidNotNull] Guid characterId, [GuidNotNull] Guid bloodPotencyId)
        {
            return
                this._bloodPotencyFactory.Convert(this._bloodPotencyToCharacterRepository.Update(characterId,
                    bloodPotencyId));
        }

        [CustomLogAspect]
        internal BloodPotencyModel SetDefault([GuidNotNull] Guid characterId)
        {
            // ReSharper disable once PossibleNullReferenceException
            return this.Update(characterId, this._bloodPotencyRepository.Get().FirstOrDefault().BloodPotencyId);
        }

        [CustomLogAspect]
        internal BloodPotencyModel Get([GuidNotNull] Guid characterId)
        {
            return this._bloodPotencyFactory.Convert(this._bloodPotencyToCharacterRepository.Get(characterId));
        }
    }
}
