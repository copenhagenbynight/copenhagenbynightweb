﻿namespace BloodPotencyLibrary.Encapsulated.Managers
{
    using System;
    using System.Collections.Generic;
    using AspectLibrary;
    using Exposed.Models;
    using Factories;
    using PostSharp.Patterns.Contracts;
    using Repositories;


    internal class BloodPotencyManager
    {
        private readonly BloodPotencyRepository _bloodPotencyRepository = new BloodPotencyRepository();
        private readonly BloodPotencyFactory _bloodPotencyConvertFactory = new BloodPotencyFactory();

        [CustomLogAspect]
        internal BloodPotencyModel Get([GuidNotNull] Guid bloodPotencyId)
        {
            return this._bloodPotencyConvertFactory.Convert(this._bloodPotencyRepository.Get(bloodPotencyId));
        }

        [CustomLogAspect]
        internal bool Delete([GuidNotNull] Guid bloodPotencyId)
        {
            return this._bloodPotencyRepository.Delete(bloodPotencyId);
        }

        [CustomLogAspect]
        internal BloodPotencyModel Add([NotNull] BloodPotencyAddModel model)
        {
            return
                this._bloodPotencyConvertFactory.Convert(
                    this._bloodPotencyRepository.Add(this._bloodPotencyConvertFactory.Convert(model)));
        }

        [CustomLogAspect]
        internal List<BloodPotencyModel> Get()
        {
            return this._bloodPotencyConvertFactory.Convert(this._bloodPotencyRepository.Get());
        }

        [CustomLogAspect]
        internal BloodPotencyModel Update([GuidNotNull] Guid bloodPotencyId, [NotNull] BloodPotencyUpdateModel model)
        {
            return
                this._bloodPotencyConvertFactory.Convert(this._bloodPotencyRepository.Update(bloodPotencyId,
                    this._bloodPotencyConvertFactory.Convert(model)));
        }
    }
}
