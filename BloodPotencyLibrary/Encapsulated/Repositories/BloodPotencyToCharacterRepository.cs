﻿namespace BloodPotencyLibrary.Encapsulated.Repositories
{
    using System;
    using System.Linq;
    using AspectLibrary;
    using DatabaseLibrary;

    internal class BloodPotencyToCharacterRepository
    {
        private readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();

        [CustomLogAspect]
        [TranslateScopeAspect(MaxRetries = 3)]
        internal BloodPotency Get([GuidNotNull] Guid characterId)
        {
            return
                this._copenhagenByNightEntities.Character.Where(item => item.CharacterId == characterId)
                    .Select(item => item.BloodPotency).FirstOrDefault();
        }

        [CustomLogAspect]
        [TranslateScopeAspect(MaxRetries = 3)]
        internal BloodPotency Update([GuidNotNull] Guid characterId, [GuidNotNull] Guid bloodPotencyId)
        {
            Character currentCharacter =
                this._copenhagenByNightEntities.Character.FirstOrDefault(item => item.CharacterId == characterId);

            // ReSharper disable once PossibleNullReferenceException
            currentCharacter.BloodPotencyId = bloodPotencyId;

            this._copenhagenByNightEntities.SaveChanges();

            return currentCharacter.BloodPotency;
        }
    }
}
