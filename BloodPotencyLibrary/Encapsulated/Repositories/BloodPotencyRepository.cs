﻿namespace BloodPotencyLibrary.Encapsulated.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AspectLibrary;
    using DatabaseLibrary;
    using PostSharp.Patterns.Contracts;

    internal class BloodPotencyRepository
    {
        private readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();

        [CustomLogAspect]
        [TranslateScopeAspect(MaxRetries = 3)]
        internal BloodPotency Get([GuidNotNull] Guid bloodPotencyId)
        {
            return
                this._copenhagenByNightEntities.BloodPotency.FirstOrDefault(
                    item => item.BloodPotencyId == bloodPotencyId);
        }

        [CustomLogAspect]
        [TranslateScopeAspect(MaxRetries = 3)]
        internal List<BloodPotency> Get()
        {
            return
                this._copenhagenByNightEntities.BloodPotency.OrderBy(item => item.BloodPotencyLevel)
                    .Select(item => item)
                    .ToList();
        }

        [CustomLogAspect]
        [TranslateScopeAspect(MaxRetries = 3)]
        public BloodPotency Update([GuidNotNull] Guid bloodPotencyId, [NotNull] BloodPotency model)
        {
            BloodPotency currentBloodPotency =
                this._copenhagenByNightEntities.BloodPotency.FirstOrDefault(
                    item => item.BloodPotencyId == bloodPotencyId);

            // ReSharper disable once PossibleNullReferenceException
            currentBloodPotency.BaseAttackBonusMax = model.BaseAttackBonusMax;
            currentBloodPotency.BloodPotencyLevel = model.BloodPotencyLevel;
            currentBloodPotency.BonusArmor = model.BonusArmor;
            currentBloodPotency.DisciplinesPrCombat = model.DisciplinesPrCombat;
            currentBloodPotency.MentalResistancePoints = model.MentalResistancePoints;
            currentBloodPotency.SustenanceId = model.SustenanceId;

            this._copenhagenByNightEntities.SaveChanges();
            return currentBloodPotency;
        }

        [CustomLogAspect]
        [TranslateScopeAspect(MaxRetries = 3)]
        internal bool Delete([GuidNotNull] Guid bloodPotencyId)
        {
            BloodPotency currentBloodPotency =
                this._copenhagenByNightEntities.BloodPotency.FirstOrDefault(
                    item => item.BloodPotencyId == bloodPotencyId);

            this._copenhagenByNightEntities.BloodPotency.Remove(currentBloodPotency);

            this._copenhagenByNightEntities.SaveChanges();

            return true;
        }

        [CustomLogAspect]
        [TranslateScopeAspect(MaxRetries = 3)]
        internal BloodPotency Add([NotNull] BloodPotency bloodPotency)
        {
            bloodPotency.BloodPotencyId = Guid.NewGuid();
            this._copenhagenByNightEntities.BloodPotency.Add(bloodPotency);

            this._copenhagenByNightEntities.SaveChanges();

            return bloodPotency;
        }
    }
}
