﻿namespace BloodPotencyLibrary.Encapsulated.Factories
{
    using System.Collections.Generic;
    using System.Linq;
    using AspectLibrary;
    using DatabaseLibrary;
    using Exposed.Models;
    using PostSharp.Patterns.Contracts;

    internal class BloodPotencyFactory
    {
        [CustomLogAspect]
        internal BloodPotencyModel Convert([NotNull] BloodPotency bloodPotency)
        {
            return new BloodPotencyModel
            {
                BloodPotencyId = bloodPotency.BloodPotencyId,
                BloodPotencyLevel = bloodPotency.BloodPotencyLevel,
                BaseAttackBonusMax = bloodPotency.BaseAttackBonusMax,
                BonusArmor = bloodPotency.BonusArmor,
                DisciplinesPrCombat = bloodPotency.DisciplinesPrCombat,
                MentalResistancePoints = bloodPotency.MentalResistancePoints,
                SustenanceId = bloodPotency.SustenanceId
            };
        }

        [CustomLogAspect]
        internal BloodPotency  Convert([NotNull] BloodPotencyModel bloodPotency)
        {
            return new BloodPotency
            {
                BloodPotencyId = bloodPotency.BloodPotencyId,
                BloodPotencyLevel = bloodPotency.BloodPotencyLevel,
                BaseAttackBonusMax = bloodPotency.BaseAttackBonusMax,
                BonusArmor = bloodPotency.BonusArmor,
                DisciplinesPrCombat = bloodPotency.DisciplinesPrCombat,
                MentalResistancePoints = bloodPotency.MentalResistancePoints,
                SustenanceId = bloodPotency.SustenanceId
            };
        }

        [CustomLogAspect]
        internal BloodPotency Convert([NotNull] BloodPotencyAddModel bloodPotency)
        {
            return new BloodPotency
            {
                BloodPotencyLevel = bloodPotency.BloodPotencyLevel,
                BaseAttackBonusMax = bloodPotency.BaseAttackBonusMax,
                BonusArmor = bloodPotency.BonusArmor,
                DisciplinesPrCombat = bloodPotency.DisciplinesPrCombat,
                MentalResistancePoints = bloodPotency.MentalResistancePoints,
                SustenanceId = bloodPotency.SustenanceId
            };
        }

        [CustomLogAspect]
        internal BloodPotency Convert([NotNull] BloodPotencyUpdateModel bloodPotency)
        {
            return new BloodPotency
            {
                BloodPotencyId = bloodPotency.BloodPotencyId,
                BloodPotencyLevel = bloodPotency.BloodPotencyLevel,
                BaseAttackBonusMax = bloodPotency.BaseAttackBonusMax,
                BonusArmor = bloodPotency.BonusArmor,
                DisciplinesPrCombat = bloodPotency.DisciplinesPrCombat,
                MentalResistancePoints = bloodPotency.MentalResistancePoints,
                SustenanceId = bloodPotency.SustenanceId
            };
        }
        internal List<BloodPotencyModel> Convert([NotNull] List<BloodPotency> listOfBloodPotencies)
        {
            return listOfBloodPotencies.Select(this.Convert).ToList();
        }
    }
}
