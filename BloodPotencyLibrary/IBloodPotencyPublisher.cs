﻿namespace BloodPotencyLibrary
{
    using System;
    using System.Collections.Generic;
    using AspectLibrary;
    using Exposed.Models;
    using PostSharp.Patterns.Contracts;

    public interface IBloodPotencyPublisher
    {
        BloodPotencyModel Add([NotNull] BloodPotencyAddModel model);
        bool Delete([GuidNotNull] Guid value);
        List<BloodPotencyModel> Get();
        BloodPotencyModel Get([GuidNotNull] Guid value);
        BloodPotencyModel Update(Guid value, [NotNull] BloodPotencyUpdateModel model);
    }
}