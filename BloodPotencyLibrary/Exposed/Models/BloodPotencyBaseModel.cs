﻿using System;
using PostSharp.Patterns.Contracts;

namespace BloodPotencyLibrary.Exposed.Models
{
    using AspectLibrary;

    public class BloodPotencyBaseModel
    {
        [StrictlyPositive]
        public int BaseAttackBonusMax { get; set; }
        [Range(1, 10)]
        public int BloodPotencyLevel { get; set; }
        [GuidNotNull]
        public Guid SustenanceId { get; set; }
        [StrictlyPositive]
        public int MentalResistancePoints { get; set; }
        [StrictlyPositive]
        public int DisciplinesPrCombat { get; set; }
        [Positive]
        public int BonusArmor { get; set; }
    }
}