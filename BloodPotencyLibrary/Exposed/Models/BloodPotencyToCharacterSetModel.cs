﻿namespace BloodPotencyLibrary.Exposed.Models
{
    using System;
    using AspectLibrary;

    public class BloodPotencyToCharacterSetModel
    {
        [GuidNotNull]
        public Guid BloodPotencyId { get; set; }
    }
}
