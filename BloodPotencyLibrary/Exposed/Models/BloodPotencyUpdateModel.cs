﻿namespace BloodPotencyLibrary.Exposed.Models
{
    using System;
    using AspectLibrary;

    public class BloodPotencyUpdateModel : BloodPotencyBaseModel
    {
        [GuidNotNull]
        public Guid BloodPotencyId { get; internal set; }
    }
}