﻿namespace BloodPotencyLibrary.Exposed.Models
{
    using System;
    using AspectLibrary;

    public class BloodPotencyModel : BloodPotencyBaseModel
    {
        [GuidNotNull]
        public Guid BloodPotencyId { get; internal set; }
    }
}