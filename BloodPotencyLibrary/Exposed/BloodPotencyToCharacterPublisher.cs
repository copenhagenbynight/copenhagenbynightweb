﻿namespace BloodPotencyLibrary.Exposed
{
    using System;
    using AspectLibrary;
    using Encapsulated.Managers;
    using Models;

    public class BloodPotencyToCharacterPublisher : IBloodPotencyToCharacterPublisher
    {
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private Guid _characterId;
        private readonly BloodPotencyToCharacterManager _bloodPotencyToCharacterManager = new BloodPotencyToCharacterManager();

        public BloodPotencyToCharacterPublisher([GuidNotNull] Guid characterId)
        {
            this._characterId = characterId;
        }

        public BloodPotencyModel Update([GuidNotNull] Guid bloodPotencyId)
        {
           return this.Update(this._characterId, bloodPotencyId);
        }

        public BloodPotencyModel SetDefault()
        {
            return this.SetDefault(this._characterId);
        }

        public BloodPotencyModel Get()
        {
            return this.Get(this._characterId);

        }

        [CacheAspect]
        private BloodPotencyModel Update([GuidNotNull] Guid characterId,[GuidNotNull] Guid bloodPotencyId)
        {
            return this._bloodPotencyToCharacterManager.Update(characterId, bloodPotencyId);
        }

        [CacheAspect]
        private BloodPotencyModel SetDefault([GuidNotNull] Guid characterId)
        {
            return this._bloodPotencyToCharacterManager.SetDefault(characterId);
        }

        [CacheAspect]
        private BloodPotencyModel Get([GuidNotNull] Guid characterId)
        {
            return this._bloodPotencyToCharacterManager.Get(characterId);

        }
    }
}
