﻿using System;
using System.Collections.Generic;
using BloodPotencyLibrary.Encapsulated.Managers;
using BloodPotencyLibrary.Exposed.Models;
using PostSharp.Patterns.Contracts;

namespace BloodPotencyLibrary.Exposed
{
    using AspectLibrary;

    public class BloodPotencyPublisher : IBloodPotencyPublisher
    {
        readonly BloodPotencyManager _bloodPotencyManager = new BloodPotencyManager();

        [CustomLogAspect]
        [CacheAspect]
        public List<BloodPotencyModel> Get()
        {
            return this._bloodPotencyManager.Get();
        }

        [CacheAspect]
        public BloodPotencyModel Get([GuidNotNull] Guid value)
        {
            return this._bloodPotencyManager.Get(value);
        }

        [CustomLogAspect]
        [CacheAspect]
        public BloodPotencyModel Add([NotNull] BloodPotencyAddModel model)
        {
            return this._bloodPotencyManager.Add(model);
        }

        [CustomLogAspect]
        [CacheAspect]
        public BloodPotencyModel Update([GuidNotNull] Guid value, [NotNull] BloodPotencyUpdateModel model)
        {
            return this._bloodPotencyManager.Update(value,model);
        }

        [CustomLogAspect]
        [CacheAspect]
        public bool Delete([GuidNotNull] Guid value)
        {
            return this._bloodPotencyManager.Delete(value);
        }
    }
}
