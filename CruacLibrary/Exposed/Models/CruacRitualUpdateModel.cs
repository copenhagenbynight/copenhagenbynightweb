﻿namespace CruacLibrary.Exposed.Models
{
    using System;
    using AspectLibrary;

    public class CruacRitualUpdateModel : CruacRitualBaseModel
    {
        [GuidNotNull]
        public Guid CruacRitualId { get; set; }
    }
}
