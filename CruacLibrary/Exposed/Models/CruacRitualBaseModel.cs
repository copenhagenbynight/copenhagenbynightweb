﻿namespace CruacLibrary.Exposed.Models
{
    using System;
    using AspectLibrary;
    using PostSharp.Patterns.Contracts;

    public class CruacRitualBaseModel
    {
        [GuidNotNull]
        public Guid CruacGroupId { get; set; }

        [StringLength(1, 249)]
        public string Name { get; set; }

        [NotEmpty]
        public string Description { get; set; }
    }
}
