﻿namespace CruacLibrary.Exposed.Models
{
    using System.Collections.Generic;

    public class CruacGroupWithRitualModel : CruacGroupModel
    {
        public List<CruacRitualModel> ListOfCruacRituas { get; set; }
    }
}
