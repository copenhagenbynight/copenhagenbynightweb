﻿namespace CruacLibrary.Exposed.Models
{
    using PostSharp.Patterns.Contracts;

    public class CruacGroupBaseModel
    {
        [StringLength(1, 249)]
        public string Name { get; set; }

        [NotEmpty]
        public string Description { get; set; }
    }
}
