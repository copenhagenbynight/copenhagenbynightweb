﻿namespace CruacLibrary.Exposed.Models
{
    using System;
    using AspectLibrary;

    public class CruacGroupUpdateModel : CruacGroupBaseModel
    {
        [GuidNotNull]
        public Guid CruacGroupId { get; set; }
    }
}
