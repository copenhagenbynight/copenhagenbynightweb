﻿namespace CruacLibrary.Exposed.Models
{
    using System;
    using AspectLibrary;

    public class CruacRitualModel : CruacRitualBaseModel
    {
        [GuidNotNull]
        public Guid CruacRitualId { get; set; }
    }
}

