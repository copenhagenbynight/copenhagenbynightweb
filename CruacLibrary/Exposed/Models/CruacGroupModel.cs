﻿namespace CruacLibrary.Exposed.Models
{
    using System;
    using AspectLibrary;

    public class CruacGroupModel : CruacGroupBaseModel
    {
        [GuidNotNull]
        public Guid CruacGroupId { get; set; }
    }
}
