﻿namespace CruacLibrary.Exposed.Publishers
{
    using System;
    using System.Collections.Generic;
    using AspectLibrary;
    using Encapsulated.Managers;
    using Models;

    public class CruacToCharacterPublisher
    {
        private readonly CruacToCharacterManager _cruacToCharacterManager = new CruacToCharacterManager();
        private readonly Guid _characterId;

        public CruacToCharacterPublisher([GuidNotNull] Guid characterId)
        {
            this._characterId = characterId;
        }

        public List<CruacGroupWithRitualModel> Get()
        {
            return this._cruacToCharacterManager.Get(this._characterId);
        }

        public CruacGroupWithRitualModel Add(Guid cruacGroupId)
        {
            return this._cruacToCharacterManager.Add(this._characterId, cruacGroupId);
        }

        public bool Delete(Guid cruacGroupId)
        {
            return this._cruacToCharacterManager.Delete(this._characterId, cruacGroupId);
        }
    }
}
