﻿namespace CruacLibrary.Exposed.Publishers
{
    using System;
    using System.Collections.Generic;
    using Encapsulated.Managers;
    using Models;

    public class CruacRitualPublisher
    {
        private readonly CruacRitualManager _cruacRitualManager = new CruacRitualManager();

        public List<CruacRitualModel> Get(Guid cruacRitualId)
        {
            return this._cruacRitualManager.Get(cruacRitualId);
        }

        public CruacRitualModel Get(Guid cruacGruopId, Guid cruacRitualId)
        {
            return this._cruacRitualManager.Get(cruacGruopId, cruacRitualId);
        }

        public CruacRitualModel Add(CruacRitualAddModel cruacRitual)
        {
            return this._cruacRitualManager.Add(cruacRitual);
        }

        public CruacRitualModel Update(Guid cruacRitualId, CruacRitualUpdateModel cruacRitual)
        {
            return this._cruacRitualManager.Update(cruacRitualId, cruacRitual);
        }

        public bool Delete(Guid cruacRitualId)
        {
            return this._cruacRitualManager.Delete(cruacRitualId);
        }
    }
}
