﻿namespace CruacLibrary.Exposed.Publishers
{
    using System;
    using System.Collections.Generic;
    using Encapsulated.Managers;
    using Models;

    public class CruacGroupPublisher
    {
        private readonly CruacGroupManager _cruacGroupManager = new CruacGroupManager();

        public List<CruacGroupModel> Get()
        {
            return this._cruacGroupManager.Get();
        }

        public CruacGroupModel Get(Guid cruacGroupId)
        {
            return this._cruacGroupManager.Get(cruacGroupId);
        }

        public CruacGroupModel Add(CruacGroupAddModel cruacGroup)
        {
            return this._cruacGroupManager.Add(cruacGroup);
        }

        public CruacGroupModel Update(Guid cruacGroupId, CruacGroupUpdateModel cruacGroup)
        {
            return this._cruacGroupManager.Update(cruacGroupId, cruacGroup);
        }

        public bool Delete(Guid cruacGroupId)
        {
            return this._cruacGroupManager.Delete(cruacGroupId);
        }
    }
}
