﻿namespace CruacLibrary.Exposed
{
    using System.Collections.Generic;
    using Models;

    public class CruacToCharacterModel
    {
        public List<CruacGroupWithRitualModel> ListOfCruacGroupWithRitual { get; set; }
    }
}
