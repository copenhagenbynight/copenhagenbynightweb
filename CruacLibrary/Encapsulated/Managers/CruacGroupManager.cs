﻿namespace CruacLibrary.Encapsulated.Managers
{
    using System;
    using System.Collections.Generic;
    using Exposed.Models;
    using Factories;
    using Repositories;

    internal class CruacGroupManager
    {
        private readonly CruacGroupRepository _cruacGroupRepository = new CruacGroupRepository();
        private readonly CruacGroupFactory _cruacGroupFactory = new CruacGroupFactory();

        internal List<CruacGroupModel> Get()
        {
            return this._cruacGroupFactory.Convert(this._cruacGroupRepository.Get());
        }

        internal CruacGroupModel Get(Guid cruacGroupId)
        {
            return this._cruacGroupFactory.Convert(this._cruacGroupRepository.Get(cruacGroupId));
        }

        internal CruacGroupModel Add(CruacGroupAddModel cruacGroup)
        {
            return
                this._cruacGroupFactory.Convert(
                    this._cruacGroupRepository.Add(this._cruacGroupFactory.Convert(cruacGroup)));
        }

        internal CruacGroupModel Update(Guid cruacGroupId, CruacGroupUpdateModel cruacGroup)
        {
            return
                this._cruacGroupFactory.Convert(this._cruacGroupRepository.Update(cruacGroupId,
                    this._cruacGroupFactory.Convert(cruacGroup)));
        }

        internal bool Delete(Guid cruacGroupId)
        {
            return this._cruacGroupRepository.Delete(cruacGroupId);
        }
    }
}
