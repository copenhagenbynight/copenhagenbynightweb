﻿namespace CruacLibrary.Encapsulated.Managers
{
    using System;
    using System.Collections.Generic;
    using Exposed.Models;
    using Factories;
    using Repositories;

    internal class CruacToCharacterManager
    {
        private readonly CruacToCharacterFactory _cruacToCharacterFactory = new CruacToCharacterFactory();
        private readonly CruacToCharacterRepository _cruacToCharacterRepository = new CruacToCharacterRepository();
        private readonly CruacRitualManager _cruacRitualManager = new CruacRitualManager();

        internal List<CruacGroupWithRitualModel> Get(Guid characterId)
        {
            List<CruacGroupWithRitualModel> result =
                this._cruacToCharacterFactory.Convert(
                    this._cruacToCharacterRepository.GetCruacGroupByCharacterId(characterId));

            for (int i = 0; i < result.Count; i++)
                result[i].ListOfCruacRituas = this._cruacRitualManager.Get(result[i].CruacGroupId);

            return result;
        }

        internal CruacGroupWithRitualModel Add(Guid characterId, Guid cruacGroupId)
        {
            CruacGroupWithRitualModel result =
                this._cruacToCharacterFactory.Convert(this._cruacToCharacterRepository.Add(characterId, cruacGroupId));

            result.ListOfCruacRituas = this._cruacRitualManager.Get(cruacGroupId);
            return result;
        }

        internal bool Delete(Guid characterId, Guid cruacGroupId)
        {
            return this._cruacToCharacterRepository.Delete(characterId, cruacGroupId);
        }
    }
}
