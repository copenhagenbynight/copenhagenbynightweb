﻿namespace CruacLibrary.Encapsulated.Managers
{
    using System;
    using System.Collections.Generic;
    using AspectLibrary;
    using Exposed.Models;
    using Factories;
    using Repositories;

    internal class CruacRitualManager
    {
        private readonly CruacRitualRepository _cruacRitualRepository = new CruacRitualRepository();
        private readonly CruacRitualFactory _cruacRitualFactory = new CruacRitualFactory();

        internal List<CruacRitualModel> Get([GuidNotNull] Guid cruacGroupId)
        {
            return this._cruacRitualFactory.Convert(this._cruacRitualRepository.Get(cruacGroupId));
        }

        internal CruacRitualModel Get([GuidNotNull] Guid cruacGroupId, [GuidNotNull] Guid cruacRitualId)
        {
            return this._cruacRitualFactory.Convert(this._cruacRitualRepository.Get(cruacGroupId, cruacRitualId));
        }

        public CruacRitualModel Add(CruacRitualAddModel cruacRitual)
        {
            return
                this._cruacRitualFactory.Convert(
                    this._cruacRitualRepository.Add(this._cruacRitualFactory.Convert(cruacRitual)));
        }

        internal CruacRitualModel Update(Guid cruacRitualId, CruacRitualUpdateModel cruacRitual)
        {
            return
                this._cruacRitualFactory.Convert(
                    this._cruacRitualRepository.Add(this._cruacRitualFactory.Convert(cruacRitual)));
        }

        internal bool Delete(Guid cruacRitualId)
        {
            return this._cruacRitualRepository.Delete(cruacRitualId);
        }
    }
}
