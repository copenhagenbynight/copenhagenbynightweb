﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CruacLibrary.Encapsulated.Repositories
{
    using AspectLibrary;
    using DatabaseLibrary;
    using Exposed.Models;
    using CruacGroup = DatabaseLibrary.CruacGroup;

    internal class CruacGroupRepository
    {
        private readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();

        internal List<CruacGroup> Get()
        {
            return (this._copenhagenByNightEntities.CruacGroup.Select(item => item)).ToList();
        }

        internal CruacGroup Get([GuidNotNull] Guid cruacGroupId)
        {
            return (this._copenhagenByNightEntities.CruacGroup.FirstOrDefault(item => item.CruacGroupId == cruacGroupId));
        }

        internal CruacGroup Add(CruacGroup cruacGroup)
        {
            this._copenhagenByNightEntities.CruacGroup.Add(cruacGroup);
            this._copenhagenByNightEntities.SaveChanges();
            return cruacGroup;
        }

        internal CruacGroup Update([GuidNotNull] Guid cruacGroupId, CruacGroup cruacGroup)
        {
            CruacGroup currentCovenant = new CruacGroup { CruacGroupId = cruacGroupId };
            this._copenhagenByNightEntities.CruacGroup.Attach(currentCovenant);

            currentCovenant.Description = cruacGroup.Description;
            currentCovenant.Name = cruacGroup.Name;

            this._copenhagenByNightEntities.SaveChanges();
            return cruacGroup;
        }

        internal bool Delete([GuidNotNull] Guid cruacGroupId)
        {
            CruacGroup currentCruacGroup = new CruacGroup { CruacGroupId = cruacGroupId };
            this._copenhagenByNightEntities.CruacGroup.Attach(currentCruacGroup);
            this._copenhagenByNightEntities.CruacGroup.Remove(currentCruacGroup);
            this._copenhagenByNightEntities.SaveChanges();
            return true;
        }
    }
}
