﻿namespace CruacLibrary.Encapsulated.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AspectLibrary;
    using DatabaseLibrary;

    internal class CruacRitualRepository
    {
        private readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();

        internal List<CruacRitual> Get([GuidNotNull] Guid cruacGroupId)
        {
            return
                this._copenhagenByNightEntities.CruacRitual.Where(item => item.CruacGroupId == cruacGroupId)
                    .Select(item => item)
                    .ToList();
        }

        internal CruacRitual Get([GuidNotNull] Guid cruacGroupId, [GuidNotNull] Guid cruacRitualId)
        {
            return
                this._copenhagenByNightEntities.CruacRitual.FirstOrDefault(
                    item => (item.CruacRitualId == cruacRitualId) && (item.CruacGroupId == cruacGroupId));
        }

        internal CruacRitual Add(CruacRitual cruacRitual)
        {
            if (
                this._copenhagenByNightEntities.CruacRitual.Count(item => item.CruacGroupId == cruacRitual.CruacGroupId) >
                10)
                throw new ArgumentOutOfRangeException($"Maximum number of ritual in group reached.");

            this._copenhagenByNightEntities.CruacRitual.Add(cruacRitual);
            this._copenhagenByNightEntities.SaveChanges();
            return cruacRitual;
        }

        internal CruacRitual Update([GuidNotNull] Guid cruacRitualId, CruacRitual cruacRitual)
        {
            CruacRitual currentCruacRitual = this._copenhagenByNightEntities.CruacRitual.FirstOrDefault(
                item => item.CruacRitualId == cruacRitualId);

            // ReSharper disable once PossibleNullReferenceException
            if ((currentCruacRitual.CruacGroupId != cruacRitualId)
                && (this._copenhagenByNightEntities.CruacRitual.Count(item => item.CruacGroupId == cruacRitual.CruacGroupId) >= 10))
                throw new ArgumentOutOfRangeException($"Maximum number of ritual in group reached.");

            currentCruacRitual.Description = cruacRitual.Description;
            currentCruacRitual.Name = cruacRitual.Name;
            currentCruacRitual.CruacRitualId = cruacRitual.CruacRitualId;

            this._copenhagenByNightEntities.SaveChanges();
            return cruacRitual;
        }

        internal bool Delete([GuidNotNull] Guid cruacRitualId)
        {
            CruacRitual currentCruacRitual = new CruacRitual {CruacRitualId = cruacRitualId};
            this._copenhagenByNightEntities.CruacRitual.Attach(currentCruacRitual);
            this._copenhagenByNightEntities.CruacRitual.Remove(currentCruacRitual);
            this._copenhagenByNightEntities.SaveChanges();
            return true;
        }
    }
}
