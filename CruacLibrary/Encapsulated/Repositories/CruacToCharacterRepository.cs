﻿namespace CruacLibrary.Encapsulated.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AspectLibrary;
    using DatabaseLibrary;

    internal class CruacToCharacterRepository
    {
        private readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();

        internal CruacGroup Add([GuidNotNull] Guid characterId, [GuidNotNull] Guid cruacGroupsId)
        {
            if (this.GetCruacGroupByCharacterId(characterId).Count >= 5)
                throw new ArgumentOutOfRangeException($"Maximum number of CruacGroup reached.");

            Character currentCharacter =
                this._copenhagenByNightEntities.Character.FirstOrDefault(item => item.CharacterId == characterId);


            CruacGroup currentCruacGroup =
                this._copenhagenByNightEntities.CruacGroup.FirstOrDefault(item => item.CruacGroupId == cruacGroupsId);

            // ReSharper disable once PossibleNullReferenceException
            currentCharacter.CruacGroup.Add(currentCruacGroup);


            this._copenhagenByNightEntities.SaveChanges();
            return currentCruacGroup;
        }

        internal bool Delete([GuidNotNull] Guid characterId, [GuidNotNull] Guid cruacGroupsId)
        {
            Character currentCharacter =
                this._copenhagenByNightEntities.Character.FirstOrDefault(item => item.CharacterId == characterId);


            CruacGroup currentCruacGroup =
                this._copenhagenByNightEntities.CruacGroup.FirstOrDefault(item => item.CruacGroupId == cruacGroupsId);

            // ReSharper disable once PossibleNullReferenceException
            currentCharacter.CruacGroup.Remove(currentCruacGroup);


            this._copenhagenByNightEntities.SaveChanges();
            return true;
        }

        internal List<CruacGroup> GetCruacGroupByCharacterId([GuidNotNull] Guid characterId)
        {
            return this._copenhagenByNightEntities.Character.Where(item => item.CharacterId == characterId)
                .Select(item => item.CruacGroup.ToList()).FirstOrDefault();
        }
    }
}
