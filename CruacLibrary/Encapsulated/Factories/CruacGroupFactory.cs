﻿namespace CruacLibrary.Encapsulated.Factories
{
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseLibrary;
    using Exposed.Models;

    internal class CruacGroupFactory
    {
        internal CruacGroup Convert(CruacGroupModel cruacGroup)
        {
            return new CruacGroup
            {
                CruacGroupId = cruacGroup.CruacGroupId,
                Name = cruacGroup.Name,
                Description = cruacGroup.Description
            };
        }

        internal CruacGroupModel Convert(CruacGroup cruacGroup)
        {
            return new CruacGroupModel
            {
                CruacGroupId = cruacGroup.CruacGroupId,
                Name = cruacGroup.Name,
                Description = cruacGroup.Description
            };
        }

        internal List<CruacGroupModel> Convert(List<CruacGroup> cruacGroupList)
        {
            return (cruacGroupList.Select(this.Convert)).ToList();
        }

        internal CruacGroup Convert(CruacGroupAddModel cruacGroup)
        {
            return new CruacGroup
            {
                Name = cruacGroup.Name,
                Description = cruacGroup.Description
            };
        }

        internal CruacGroup Convert(CruacGroupUpdateModel cruacGroup)
        {
            return new CruacGroup
            {
                CruacGroupId = cruacGroup.CruacGroupId,
                Name = cruacGroup.Name,
                Description = cruacGroup.Description
            };
        }
    }
}
