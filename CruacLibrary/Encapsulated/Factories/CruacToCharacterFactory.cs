﻿namespace CruacLibrary.Encapsulated.Factories
{
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseLibrary;
    using Exposed.Models;

    internal class CruacToCharacterFactory
    {
        internal CruacGroupWithRitualModel Convert(CruacGroup item)
        {
            return new CruacGroupWithRitualModel
            {
                CruacGroupId = item.CruacGroupId,
                Name = item.Name,
                Description = item.Description
            };
        }

        internal List<CruacGroupWithRitualModel> Convert(List<CruacGroup> list)
        {
            return list.Select(this.Convert).ToList();
        }
    }
}
