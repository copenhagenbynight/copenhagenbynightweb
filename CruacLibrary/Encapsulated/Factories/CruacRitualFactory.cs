﻿namespace CruacLibrary.Encapsulated.Factories
{
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseLibrary;
    using Exposed.Models;

    internal class CruacRitualFactory
    {
        internal CruacRitualModel Convert(CruacRitual cruacRitual)
        {
            return new CruacRitualModel
            {
                CruacRitualId = cruacRitual.CruacRitualId,
                Name = cruacRitual.Name,
                Description = cruacRitual.Description,
                CruacGroupId = cruacRitual.CruacGroupId
            };
        }

        internal CruacRitual Convert(CruacRitualModel cruacRitual)
        {
            return new CruacRitual
            {
                CruacRitualId = cruacRitual.CruacRitualId,
                Name = cruacRitual.Name,
                Description = cruacRitual.Description,
                CruacGroupId = cruacRitual.CruacGroupId
            };
        }

        internal List<CruacRitualModel> Convert(List<CruacRitual> cruacRitualList)
        {
            return (cruacRitualList.Select(this.Convert)).ToList();
        }

        internal CruacRitual Convert(CruacRitualAddModel cruacRitual)
        {
            return new CruacRitual
            {
                Name = cruacRitual.Name,
                Description = cruacRitual.Description,
                CruacGroupId = cruacRitual.CruacGroupId
            };
        }

        internal CruacRitual Convert(CruacRitualUpdateModel cruacRitual)
        {
            return new CruacRitual
            {
                CruacRitualId = cruacRitual.CruacRitualId,
                Name = cruacRitual.Name,
                Description = cruacRitual.Description,
                CruacGroupId = cruacRitual.CruacGroupId
            };
        }
    }
}
