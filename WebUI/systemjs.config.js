﻿/**
 * System configuration for Angular 2 samples
 * Adjust as necessary for your application needs.
 */
(function (global) {
    // custom implementation for System cache busting
    var systemLocate = System.locate;
    System.locate = function (load) {
        var System = this; // its good to ensure exact instance-binding
        return Promise.resolve(systemLocate.call(this, load)).then(function (address) {
            return address + "?" + System.cacheBust;
        });
    }

    // map tells the System loader where to look for things
    var map = {
        'app': '/RuntimeScripts/', // 'dist',
        '@angular': '/RuntimeScripts/libraries/@angular',
        'rxjs': '/RuntimeScripts/libraries/rxjs',
        'ng2-translate': '/RuntimeScripts/libraries/ng2-translate',
        'primeng': '/RuntimeScripts/libraries/primeng',
        'dragula': '/RuntimeScripts/libraries/dragula/dist/dragula.min.js',
        'ng2-dragula': '/RuntimeScripts/libraries/ng2-dragula'
    };

    // packages tells the System loader how to load when no filename and/or no extension
    var packages = {
        'app': { main: 'main.js', defaultExtension: 'js' },
        'rxjs': { defaultExtension: 'js' },
        'ng2-translate': { defaultExtension: 'js' },
        'primeng': { defaultExtension: 'js' },
        'dragula': { defaultExtension: 'js' },
        'ng2-dragula': { defaultExtension: 'js' }
    };

    // packages tells the System loader how to load when no filename and/or no extension
    var ngPackageNames = [
      'common',
      'compiler',
      'core',
      'forms',
      'http',
      'platform-browser',
      'platform-browser-dynamic',
      'router',
      'router-deprecated',
      'upgrade'
    ];

    // Individual files (~300 requests):
    function packIndex(pkgName) {
        packages['@angular/' + pkgName] = { main: 'index.js', defaultExtension: 'js' };
    }

    // Bundled (~40 requests):
    function packUmd(pkgName) {
        packages['@angular/' + pkgName] = { main: 'bundles/' + pkgName + '.umd.js', defaultExtension: 'js' };
    }

    // Most environments should use UMD; some (Karma) need the individual index files
    var setPackageConfig = System.packageWithIndex ? packIndex : packUmd;

    // Add package entries for angular packages
    ngPackageNames.forEach(setPackageConfig);
    var config = {
        map: map,
        packages: packages
    };
    System.config(config);
    System.cacheBust = window.serverVariables.urlArgs;
})(this);