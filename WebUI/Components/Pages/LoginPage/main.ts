///<reference path="./../../../typings/globals/core-js/index.d.ts"/>
"use strict";
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './login-page.module';
platformBrowserDynamic().bootstrapModule(AppModule);