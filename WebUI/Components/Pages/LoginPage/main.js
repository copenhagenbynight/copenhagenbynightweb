///<reference path="./../../../typings/globals/core-js/index.d.ts"/>
"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var login_page_module_1 = require('./login-page.module');
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(login_page_module_1.AppModule);
