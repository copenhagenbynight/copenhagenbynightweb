/// <binding BeforeBuild='projectBuild' Clean='projectClean' ProjectOpened='projectOpenWithWatch' />

var ts = require('gulp-typescript');
var gulp = require('gulp');
var clean = require('gulp-clean');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var minifyCss = require('gulp-minify-css');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var replace = require('gulp-replace-task');

// Setting up base destinations
var destPathLibraries = './RuntimeScripts/Libraries/';
var destPathAppScripts = './RuntimeScripts/';
var timestamp = ((new Date()).getTime());

// Run all clean* tasks
gulp.task('cleanAll', ['cleanLibraries', 'cleanScripts']);

// Delete all file in ./RuntimeScripts/Libraries/
gulp.task('cleanLibraries', function () {
    return gulp.src(destPathLibraries).pipe(clean());
});

// Delete all file in ./RuntimeScripts/appScripts/
gulp.task('cleanScripts', function () {
    return gulp.src(destPathAppScripts).pipe(clean());
});

// Copy all file we need from node_modules to ./RuntimeScripts/Libraries/
gulp.task("copyLibraries", function () {
    return gulp.src([
         'core-js/client/**',
         'zone.js/dist/**',
         'reflect-metadata/**',
         'systemjs/dist/**',
         'rxjs/**',
         '@angular/**',
         'ng2-translate/**',
         'primeng/**',
         'underscore/**',
         'dragula/**',
         'ng2-dragula/**'
    ], { cwd: "node_modules/**" })
     .pipe(gulp.dest(destPathLibraries));
});


// Compile TypeScript in Components and put JavaScript file in ./RuntimeScripts/appScripts/
var tsProject = ts.createProject('./tsconfig.json');
gulp.task('compileTypeScript', [], function () {
    var tsResult = gulp.src(["./Components/*.ts", "./Components/**/*.ts"])
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(tsProject(ts.reporter.fullReporter()));
    return tsResult.js.pipe(sourcemaps.write('./')).pipe(gulp.dest(destPathAppScripts));
});


gulp.task('compileTypeScriptWithCacheBust', ['compileTypeScript'], function () {
    return gulp.src(["./RuntimeScripts/**/Pages/**/*.component.js"])
       .pipe(replace({
           patterns: [
             {
                 match: /.html/g,
                 replacement: '.html?v=' + timestamp
             }
           ]
       }))
       .pipe(replace({
           patterns: [
             {
                 match: /\.css']/g,
                 replacement: '.css?v=' + timestamp + "']"
             }
           ]
       }))
       .pipe(gulp.dest('./RuntimeScripts'));
});
// Copy html in Components and put them in ./RuntimeScripts/appScripts/
gulp.task('copyHtml', function () {
    return gulp.src(["./Components/*.html", "./Components/**/*.html"])
      .pipe(gulp.dest(destPathAppScripts));
});

gulp.task('copyResx', function () {
    return gulp.src(["./Components/*.resx", "./Components/**/*.resx"])
      .pipe(gulp.dest(destPathAppScripts));
});
// Copy all images in ./Components/Content/Images/ and put them in ./RuntimeScripts/Content/Images/
gulp.task('copyImagesAll', ['copyImagesSvg', 'copyImagesPng', 'copyImagesGif']);

// Copy .svg images in ./Components/Content/Images/ and put them in ./RuntimeScripts/Content/Images/
gulp.task('copyImagesSvg', function () {
    return gulp.src(["./Components/Content/Images/*.svg", "./Components/Content/Images/**/*.svg"])
  .pipe(gulp.dest(destPathAppScripts + 'Content/Images/'));
});

// Copy .png images in ./Components/Content/Images/ and put them in ./RuntimeScripts/Content/Images/
gulp.task('copyImagesPng', function () {
    return gulp.src(["./Components/Content/Images/*.png", "./Components/Content/Images/**/*.png"])
      .pipe(gulp.dest(destPathAppScripts + 'Content/Images/'));
});

// Copy .gif images in ./Components/Content/Images/ and put them in ./RuntimeScripts/Content/Images/
gulp.task('copyImagesGif', function () {
    return gulp.src(["./Components/Content/Images/*.gif", "./Components/Content/Images/**/*.gif"])
      .pipe(gulp.dest(destPathAppScripts + 'Content/Images/'));
});

// Compile scss in Components and put JavaScript file in ./RuntimeScripts/appScripts/
gulp.task('compileScss', function () {
    return gulp.src('./Components/**/*.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer({
          browsers: ['last 3 versions']
      }))
      .pipe(minifyCss({ compatibility: 'ie8' }))
      .pipe(gulp.dest(destPathAppScripts));
});

//Keep a watch on file change in Components folder and automatically execure relevent tasks when something is changed
gulp.task('watchAll', [], function () {
    gulp.watch(['Components/*.ts', 'Components/**/*.ts'], ['compileTypeScriptWithCacheBust']);
    gulp.watch(['Components/*.scss', 'Components/**/*.scss'], ['compileScss']);
    gulp.watch(['Components/*.html', 'Components/**/*.html'], ['copyHtml']);
    gulp.watch(["Components/Content/Images/*.svg", "Components/Content/Images/**/*.svg"], ['copyImagesSvg']);

});

// Will execute all copy, compile tasks and startup the watch when project open
gulp.task('projectOpenWithWatch', ['copyLibraries', 'compileTypeScriptWithCacheBust', 'copyHtml', 'copyResx', 'copyImagesAll', 'compileScss', 'watchAll']);

// Will copy, compile tasks except copyLibraries on project build.
gulp.task('projectBuild', ['compileTypeScriptWithCacheBust', 'copyHtml', 'copyResx', 'copyImagesAll', 'compileScss']);

gulp.task('projectReBuild', ['copyLibraries', 'compileTypeScriptWithCacheBust', 'copyHtml', 'copyResx', 'copyImagesAll', 'compileScss']);

// Will Delete all ./Scripts/Libraries/ and then add back libraries.
gulp.task('projectClean', ['cleanAll', 'copyLibraries']);