﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AccountRolesLibrary.Exposed.Models;
using AccountRolesLibrary.Exposed.Publishers;
using PostSharp.Patterns.Contracts;

namespace webUI.Controllers
{


    [Authorize(Roles = "Administrator")]
    public class AccountRolesController : ApiController
    {
        readonly RolePublisher _rolePublisher = new RolePublisher();
        // GET: api/AccountRoles
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, this._rolePublisher.Get());
        }

        // GET: api/AccountRoles/5
        public HttpResponseMessage Get(Guid id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, this._rolePublisher.Get(id));
        }

        // POST: api/AccountRoles
        public HttpResponseMessage Post([NotNull] RoleAddModel value)
        {
            this._rolePublisher.Add(value);
            return Request.CreateResponse(HttpStatusCode.OK,"Completed");
        }

        // PUT: api/AccountRoles/5
        public HttpResponseMessage Put(Guid id, [NotNull] RoleUpdateModel value)
        {
            this._rolePublisher.Update(id,value);
            return Request.CreateResponse(HttpStatusCode.OK, "Completed");
        }

        // DELETE: api/AccountRoles/5
        public HttpResponseMessage Delete(Guid id)
        {
            this._rolePublisher.Delete(id);
            return Request.CreateResponse(HttpStatusCode.OK, "Completed");
        }
    }
}
