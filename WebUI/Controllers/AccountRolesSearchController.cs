﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AccountRolesLibrary.Exposed.Publishers;

namespace webUI.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AccountRolesSearchController : ApiController
    {
        readonly RolePublisherWithSearch _rolePublisherWithSearch = new RolePublisherWithSearch();
        // GET: api/AccountRolesSearch
        public HttpResponseMessage Get()
        {
            throw new NotImplementedException();
        }

        // GET: api/AccountRolesSearch/5
        public HttpResponseMessage Get(string id)
        {
           return  Request.CreateResponse(HttpStatusCode.OK, _rolePublisherWithSearch.Get(id));
        }

        // POST: api/AccountRolesSearch
        public HttpResponseMessage Post([FromBody]string value)
        {
            throw new NotImplementedException();
        }

        // PUT: api/AccountRolesSearch/5
        public HttpResponseMessage Put(int id, [FromBody]string value)
        {
            throw new NotImplementedException();
        }

        // DELETE: api/AccountRolesSearch/5
        public HttpResponseMessage Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
