﻿namespace webUI.Controllers.WebApi.Clan
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using AspectLibrary;
    using ClanLibrary;
    using ClanLibrary.Exposed.Models;
    using ClanLibrary.Exposed.Publishers;

    public class ClanController : ApiController
    {
        private readonly IClanPublisher _publisher = new ClanPublisher();
        // GET: api/Clan
        [AllowAnonymous]
        [HttpResponseMessageErrorAdjustmentAspect]
        public HttpResponseMessage Get()
        {
            List<ClanModel> result = this._publisher.Get();
            return this.Request.CreateResponse(HttpStatusCode.OK,result );
        }

        // GET: api/Clan/5
        [AllowAnonymous]
        [HttpResponseMessageErrorAdjustmentAspect]
        public HttpResponseMessage Get(string id)
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Get(Guid.Parse(id)));
        }

        // POST: api/Clan
        [AllowAnonymous]
        [HttpResponseMessageErrorAdjustmentAspect]
        public HttpResponseMessage Post(ClanAddModel value)
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Add(value));
        }

        // PUT: api/Clan/5
        [AllowAnonymous]
        [HttpResponseMessageErrorAdjustmentAspect]
        public HttpResponseMessage Put(string id, ClanUpdateModel value)
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Update(Guid.Parse(id), value));
        }

        // DELETE: api/Clan/5
        [AllowAnonymous]
        [HttpResponseMessageErrorAdjustmentAspect]
        public HttpResponseMessage Delete(string id)
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Delete(Guid.Parse(id)));
        }
    }
}
