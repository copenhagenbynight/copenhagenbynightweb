﻿namespace webUI.Controllers.WebApi.Clan
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using AspectLibrary;
    using ClanLibrary;
    using ClanLibrary.Exposed.Models;
    using ClanLibrary.Exposed.Publishers;

    public class ClanToCharacterController : ApiController
    {
        private IClanToCharacterPublisher _publisher;

        // GET: api/BloodPotencyToCharacter/5
        [Authorize]
        [HttpResponseMessageErrorAdjustmentAspect]
        public HttpResponseMessage Get(string id)
        {
            this._publisher = new ClanToCharacterPublisher(Guid.Parse(id));
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Get());
        }

        // PUT: api/BloodPotencyToCharacter/5
        [Authorize]
        [HttpResponseMessageErrorAdjustmentAspect]
        public HttpResponseMessage Put(string id, ClanToCharacterSetModel value)
        {
            this._publisher = new ClanToCharacterPublisher(Guid.Parse(id));
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Update(value.ClanId));
        }
    }
}
