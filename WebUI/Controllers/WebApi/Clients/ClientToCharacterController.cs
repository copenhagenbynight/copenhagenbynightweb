﻿namespace webUI.Controllers.WebApi.Clients
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using AspectLibrary;
    using ClientsLibrary;
    using ClientsLibrary.Exposed;
    using ClientsLibrary.Exposed;
    using ClientsLibrary.Exposed.Models;

    public class ClientToCharacterController : ApiController
    {
        private IClientsToCharacterPublisher _publisher;

        // GET: api/BloodPotencyToCharacter/5
        [Authorize]
        [HttpResponseMessageErrorAdjustmentAspectAttribute]
        public HttpResponseMessage Get(string id)
        {
            this._publisher = new ClientsToCharacterPublisher(Guid.Parse(id));
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Get());
        }

        // PUT: api/BloodPotencyToCharacter/5
        [Authorize]
        [HttpResponseMessageErrorAdjustmentAspectAttribute]
        public HttpResponseMessage Put(string id, ClientsToCharacterSetModel value)
        {
            this._publisher = new ClientsToCharacterPublisher(Guid.Parse(id));
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Update(value.ClientId));
        }
    }
}
