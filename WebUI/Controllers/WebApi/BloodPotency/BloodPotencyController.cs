﻿namespace webUI.Controllers.WebApi.BloodPotency
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using AspectLibrary;
    using BloodPotencyLibrary;
    using BloodPotencyLibrary.Exposed;
    using BloodPotencyLibrary.Exposed.Models;

    public class BloodPotencyController : ApiController
    {
        private readonly IBloodPotencyPublisher _publisher = new BloodPotencyPublisher();
        // GET: api/BloodPotency
        [Authorize]
        [HttpResponseMessageErrorAdjustmentAspect]
        public HttpResponseMessage Get()
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Get());
        }

        // GET: api/BloodPotency/5
        [Authorize]
        [HttpResponseMessageErrorAdjustmentAspect]
        public HttpResponseMessage Get(string id)
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Get(Guid.Parse(id)));
        }

        // POST: api/BloodPotency
        [Authorize(Roles = "Administrator")]
        [HttpResponseMessageErrorAdjustmentAspect]
        public HttpResponseMessage Post(BloodPotencyAddModel value)
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Add(value));
        }

        // PUT: api/BloodPotency/5
        [Authorize(Roles = "Administrator")]
        [HttpResponseMessageErrorAdjustmentAspect]
        public HttpResponseMessage Put(string id, BloodPotencyUpdateModel value)
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Update(Guid.Parse(id), value));
        }

        // DELETE: api/BloodPotency/5
        [Authorize(Roles = "Administrator")]
        [HttpResponseMessageErrorAdjustmentAspect]
        public HttpResponseMessage Delete(string id)
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Delete(Guid.Parse(id)));
        }
    }
}
