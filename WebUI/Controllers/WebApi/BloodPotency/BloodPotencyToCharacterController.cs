﻿namespace webUI.Controllers.WebApi.BloodPotency
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using AspectLibrary;
    using BloodPotencyLibrary;
    using BloodPotencyLibrary.Exposed;
    using BloodPotencyLibrary.Exposed.Models;

    public class BloodPotencyToCharacterController : ApiController
    {
        private IBloodPotencyToCharacterPublisher _publisher;

        // GET: api/BloodPotencyToCharacter/5
        [Authorize]
        [HttpResponseMessageErrorAdjustmentAspect]
        public HttpResponseMessage Get(string id)
        {
            this._publisher = new BloodPotencyToCharacterPublisher(Guid.Parse(id));
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Get());
        }

        // PUT: api/BloodPotencyToCharacter/5
        [Authorize]
        [HttpResponseMessageErrorAdjustmentAspect]
        public HttpResponseMessage Put(string id, BloodPotencyToCharacterSetModel value)
        {
            this._publisher = new BloodPotencyToCharacterPublisher(Guid.Parse(id));
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Update(value.BloodPotencyId));
        }
    }
}
