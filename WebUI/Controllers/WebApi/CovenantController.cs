﻿namespace webUI.Controllers.WebApi
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using AspectLibrary;
    using CovenantsLibrary.Exposed;
    using CovenantsLibrary.Exposed.Models;

    public class CovenantController : ApiController
    {
        private readonly CovenantsPublisher _publisher = new CovenantsPublisher();
        // GET: api/Clan
        [Authorize]
        [HttpResponseMessageErrorAdjustmentAspectAttribute]
        public HttpResponseMessage Get()
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Get());
        }

        // GET: api/Clan/5
        [Authorize(Roles = "Administrator")]
        [HttpResponseMessageErrorAdjustmentAspectAttribute]
        public HttpResponseMessage Get(string id)
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Get(Guid.Parse(id)));
        }

        // POST: api/Clan
        [Authorize(Roles = "Administrator")]
        [HttpResponseMessageErrorAdjustmentAspectAttribute]
        public HttpResponseMessage Post(CovenantsAddModel value)
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Add(value));
        }

        // PUT: api/Clan/5
        [Authorize(Roles = "Administrator")]
        [HttpResponseMessageErrorAdjustmentAspectAttribute]
        public HttpResponseMessage Put(string id, CovenantsUpdateModel value)
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Update(Guid.Parse(id), value));
        }

        // DELETE: api/Clan/5
        [Authorize(Roles = "Administrator")]
        [HttpResponseMessageErrorAdjustmentAspectAttribute]
        public HttpResponseMessage Delete(string id)
        {
            return this.Request.CreateResponse(HttpStatusCode.OK, this._publisher.Delete(Guid.Parse(id)));
        }
    }
}
