﻿export class BloodPotencyModel {
    constructor(
// ReSharper disable InconsistentNaming
        public BloodPotencyId: string,
        public BloodPotencyLevel: number,
        public BaseAttackBonusMax: number,
        public BonusArmor: number,
        public DisciplinesPrCombat: number,
        public MentalResistancePoints: number, 
        public SustenanceId: string) { }
// ReSharper restore InconsistentNaming

}
