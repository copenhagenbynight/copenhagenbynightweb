﻿export class ClanModel {
    constructor(
        // ReSharper disable InconsistentNaming
        public ClanId: string,
        public Name: string,
        public Description: string,
    ) { }
    // ReSharper restore InconsistentNaming

}