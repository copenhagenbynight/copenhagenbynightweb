﻿import { Injectable, EventEmitter } from '@angular/core';
import { Headers, Http, Response } from "@angular/http";
import 'rxjs/Rx';

@Injectable()
export class IdentificationService {
    constructor(private http: Http) { }

    public header() {
        return new Headers({
            'Content-Type': 'application/json'
        });

    }
}
