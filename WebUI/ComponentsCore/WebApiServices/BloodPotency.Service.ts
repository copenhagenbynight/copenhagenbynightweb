﻿import { Injectable, EventEmitter } from '@angular/core';
import { Headers, Http, Response } from "@angular/http";
import 'rxjs/Rx';

import { BloodPotencyModel } from "../Model/BloodPotencyModel";
import { AnnouncementService } from "../Announcement.Service";
import { IdentificationService } from "../Identification.Service";

@Injectable()
export class BloodPotencyService {

    public bloodPotencyChanged = new EventEmitter<BloodPotencyModel[]>();

    private listOfBloodProteins: BloodPotencyModel[];

    constructor(private http: Http, private announcementService: AnnouncementService, private identificationService: IdentificationService) { }

    public get() {
            return this.listOfBloodProteins;
    }

    public delete(bloodPotency: BloodPotencyModel) {
        this.listOfBloodProteins.splice(this.listOfBloodProteins.indexOf(bloodPotency), 1);
        this.apiDelete(bloodPotency);
    }

    public add(bloodPotency: BloodPotencyModel) {
        this.listOfBloodProteins.push(bloodPotency);
        this.apiCreate(bloodPotency);
    }

    public update(oldBloodPotency: BloodPotencyModel, newBloodPotency: BloodPotencyModel) {
        this.listOfBloodProteins[this.listOfBloodProteins.indexOf(oldBloodPotency)] = newBloodPotency;
        this.apiUpdate(newBloodPotency);
    }

    // Below private metods
    private apiCreate(data: BloodPotencyModel) {
        const body = JSON.stringify({
            BloodPotencyLevel: data.BloodPotencyLevel,
            BaseAttackBonusMax: data.BaseAttackBonusMax,
            BonusArmor: data.BonusArmor,
            DisciplinesPrCombat: data.DisciplinesPrCombat,
            MentalResistancePoints: data.MentalResistancePoints,
            SustenanceId: data.SustenanceId
        });
        let headers = this.identificationService.header();
        return this.http.put('/api/BloodPotency', body, { headers: headers })
            .map((response: Response) => response.json())
            .subscribe(() => {
                this.apiRead();
            }, (data) => {
                this.announcementService.failure(data);
            });
    };

    private apiRead() {
        let headers = this.identificationService.header();
        return this.http.get('/api/BloodPotency', { headers: headers })
            .map((response: Response) => response.json())
            .subscribe(
            (data: BloodPotencyModel[]) => {
                this.listOfBloodProteins = data;
                this.bloodPotencyChanged.emit(this.listOfBloodProteins);
            },(data) => {
                this.announcementService.failure(data);
                }
            );
    }

    private apiUpdate(data: BloodPotencyModel) {
        let headers = this.identificationService.header();
        const body = JSON.stringify(data);
        return this.http.put('/api/BloodPotency' + data.BloodPotencyId, body, { headers: headers })
            .map((response: Response) => response.json())
            .subscribe(() => {
                this.apiRead();
            }, (data) => {
                this.announcementService.failure(data);
            });
    };

    private apiDelete(data: BloodPotencyModel) {
        let headers = this.identificationService.header();
        return this.http.delete('/api/BloodPotency' + data.BloodPotencyId, { headers: headers })
            .map((response: Response) => response.json())
            .subscribe(() => {
                this.apiRead();
            },(data) => {
                this.announcementService.failure(data);
            } );
    };
}
