//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DatabaseLibrary
{
    using System;
    using System.Collections.Generic;
    
    public partial class MysterieGroup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MysterieGroup()
        {
            this.MysterieCoil = new HashSet<MysterieCoil>();
            this.MysterieCoilToCharacter = new HashSet<MysterieCoilToCharacter>();
            this.MysterieScale = new HashSet<MysterieScale>();
        }
    
        public System.Guid MysterieGroupId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MysterieCoil> MysterieCoil { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MysterieCoilToCharacter> MysterieCoilToCharacter { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MysterieScale> MysterieScale { get; set; }
    }
}
