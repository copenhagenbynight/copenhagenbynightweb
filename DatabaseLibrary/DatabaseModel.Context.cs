﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DatabaseLibrary
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class CopenhagenByNightEntities : DbContext
    {
        public CopenhagenByNightEntities()
            : base("name=CopenhagenByNightEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<BloodPotency> BloodPotency { get; set; }
        public virtual DbSet<Character> Character { get; set; }
        public virtual DbSet<CharacterBackground> CharacterBackground { get; set; }
        public virtual DbSet<CharacterSecret> CharacterSecret { get; set; }
        public virtual DbSet<CharacterTouchstones> CharacterTouchstones { get; set; }
        public virtual DbSet<Clan> Clan { get; set; }
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<ClientResourceToCharacter> ClientResourceToCharacter { get; set; }
        public virtual DbSet<ClientToCharacter> ClientToCharacter { get; set; }
        public virtual DbSet<Covenant> Covenant { get; set; }
        public virtual DbSet<CruacGroup> CruacGroup { get; set; }
        public virtual DbSet<CruacRitual> CruacRitual { get; set; }
        public virtual DbSet<Discipline> Discipline { get; set; }
        public virtual DbSet<DisciplinesRank> DisciplinesRank { get; set; }
        public virtual DbSet<DisciplinesToCharacter> DisciplinesToCharacter { get; set; }
        public virtual DbSet<Humanity> Humanity { get; set; }
        public virtual DbSet<MysterieCoil> MysterieCoil { get; set; }
        public virtual DbSet<MysterieCoilToCharacter> MysterieCoilToCharacter { get; set; }
        public virtual DbSet<MysterieGroup> MysterieGroup { get; set; }
        public virtual DbSet<MysterieScale> MysterieScale { get; set; }
        public virtual DbSet<Network> Network { get; set; }
        public virtual DbSet<Resource> Resource { get; set; }
        public virtual DbSet<ResourcePoint> ResourcePoint { get; set; }
        public virtual DbSet<ResourcePointToCharacter> ResourcePointToCharacter { get; set; }
        public virtual DbSet<ResourceToCharacter> ResourceToCharacter { get; set; }
        public virtual DbSet<ResourceType> ResourceType { get; set; }
        public virtual DbSet<Sustenance> Sustenance { get; set; }
        public virtual DbSet<database_firewall_rules> database_firewall_rules { get; set; }
    
        public virtual ObjectResult<ProcedureDisciplinesToCharacter_Result> ProcedureDisciplinesToCharacter(Nullable<System.Guid> characterId)
        {
            var characterIdParameter = characterId.HasValue ?
                new ObjectParameter("CharacterId", characterId) :
                new ObjectParameter("CharacterId", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<ProcedureDisciplinesToCharacter_Result>("ProcedureDisciplinesToCharacter", characterIdParameter);
        }
    
        public virtual int DisciplinesToCharacterInsertOrUdpate(Nullable<System.Guid> disciplineId, Nullable<System.Guid> characterId, Nullable<int> disciplineLevel)
        {
            var disciplineIdParameter = disciplineId.HasValue ?
                new ObjectParameter("DisciplineId", disciplineId) :
                new ObjectParameter("DisciplineId", typeof(System.Guid));
    
            var characterIdParameter = characterId.HasValue ?
                new ObjectParameter("CharacterId", characterId) :
                new ObjectParameter("CharacterId", typeof(System.Guid));
    
            var disciplineLevelParameter = disciplineLevel.HasValue ?
                new ObjectParameter("DisciplineLevel", disciplineLevel) :
                new ObjectParameter("DisciplineLevel", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DisciplinesToCharacterInsertOrUdpate", disciplineIdParameter, characterIdParameter, disciplineLevelParameter);
        }
    
        public virtual ObjectResult<ProcedureGetDisciplinesForCharacterByCharacterId_Result> ProcedureGetDisciplinesForCharacterByCharacterId(Nullable<System.Guid> characterId)
        {
            var characterIdParameter = characterId.HasValue ?
                new ObjectParameter("CharacterId", characterId) :
                new ObjectParameter("CharacterId", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<ProcedureGetDisciplinesForCharacterByCharacterId_Result>("ProcedureGetDisciplinesForCharacterByCharacterId", characterIdParameter);
        }
    }
}
