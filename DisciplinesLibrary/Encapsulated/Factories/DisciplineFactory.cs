﻿namespace DisciplinesLibrary.Encapsulated.Factories
{
    using System.Collections.Generic;
    using System.Linq;
    using AspectLibrary;
    using DatabaseLibrary;
    using Exposed.Models;
    using PostSharp.Patterns.Contracts;

    internal class DisciplineFactory
    {
        [CustomLogAspect]
        internal List<DisciplineModel> Convert([NotNull] List<Discipline> listOfDisciplines)
        {
            return listOfDisciplines.Select(this.Convert).ToList();
        }

        [CustomLogAspect]
        internal DisciplineModel Convert([NotNull] Discipline item)
        {
            return new DisciplineModel
            {
                DisciplineId = item.DisciplineId,
                Name = item.Name,
                Description = item.Description
            };
        }

        [CustomLogAspect]
        internal Discipline Convert([NotNull] DisciplineUpdateModel item)
        {
            return new Discipline
            {
                DisciplineId = item.DisciplineId,
                Name = item.Name,
                Description = item.Description
            };
        }

        [CustomLogAspect]
        internal Discipline Convert([NotNull] DisciplineAddModel item)
        {
            return new Discipline
            {
                Name = item.Name,
                Description = item.Description
            };
        }

        [CustomLogAspect]
        internal Discipline Convert([NotNull] DisciplineModel item)
        {
            return new Discipline
            {
                DisciplineId = item.DisciplineId,
                Name = item.Name,
                Description = item.Description
            };
        }
    }
}
