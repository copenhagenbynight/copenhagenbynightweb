﻿using System.Collections.Generic;
using System.Linq;
using DatabaseLibrary;
using DisciplinesLibrary.Exposed.Models;

namespace DisciplinesLibrary.Encapsulated.Factories
{
    class DisciplinesToCharacterFactory
    {
        internal CharacterDisciplineModel Convert(DisciplinesToCharacter item)
        {
            return new CharacterDisciplineModel
            {
                DisciplineId = item.DisciplineId,
                Name = item.Discipline.Name,
                Level = item.DisciplineLevel
            };
        }

        public bool Contains(List<Discipline> listOfClanDisciplines, DisciplinesToCharacter item)
        {
            return listOfClanDisciplines.Contains(new Discipline
            {
                DisciplineId = item.DisciplineId
            }, new DisciplinesToCharacterFactoryCompare());
        }
    }

    internal class DisciplinesToCharacterFactoryCompare : IEqualityComparer<Discipline>
    {
        public bool Equals(Discipline x, Discipline y)
        {
            return x.DisciplineId == y.DisciplineId;
        }

        public int GetHashCode(Discipline obj)
        {
            return obj.DisciplineId.GetHashCode();
        }
    }
}
