﻿namespace DisciplinesLibrary.Encapsulated.Factories
{
    using System.Collections.Generic;
    using System.Linq;
    using AspectLibrary;
    using DatabaseLibrary;
    using Exposed;
    using Exposed.Models;

    internal class DisciplinesRankFactory
    {
        [CustomLogAspect]
        internal DisciplinesRank Convert(DisciplinesRankCreateModel model)
        {
            return new DisciplinesRank
            {
                Name = model.Name,
                Description = model.Description,
                DisciplineId = model.DisciplineId
            };
        }

        [CustomLogAspect]
        internal DisciplinesRankModel Convert(DisciplinesRank model)
        {
            return new DisciplinesRankModel
            {
                Name = model.Name,
                Description = model.Description,
                DisciplineId = model.DisciplineId,
                DisciplinesRankId = model.DisciplinesRankId
            };
        }

        [CustomLogAspect]
        internal List<DisciplinesRankModel> Convert(List<DisciplinesRank> list)
        {
            return list.Select(this.Convert).ToList();
        }

        [CustomLogAspect]
        internal DisciplinesRank Convert(DisciplinesRankUpdateModel model)
        {
            return new DisciplinesRank
            {
                Name = model.Name,
                Description = model.Description,
                DisciplineId = model.DisciplineId,
                DisciplinesRankId = model.DisciplinesRankId
            };
        }
    }
}
