﻿namespace DisciplinesLibrary.Encapsulated.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AspectLibrary;
    using DatabaseLibrary;
    using PostSharp.Patterns.Contracts;

    internal class DisciplineRepository
    {
        private readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();

        [CustomLogAspect]
        [TranslateScopeAspect(MaxRetries = 3)]
        internal List<Discipline> Get()
        {
            return this._copenhagenByNightEntities.Discipline.Select(item => item).ToList();
        }

        [CustomLogAspect]
        [TranslateScopeAspect(MaxRetries = 3)]
        internal Discipline Get([GuidNotNull] Guid disciplineId)
        {
            return
                this._copenhagenByNightEntities.Discipline.FirstOrDefault(item => item.DisciplineId == disciplineId);
        }

        [CustomLogAspect]
        [TranslateScopeAspect(MaxRetries = 3)]
        internal Discipline Add([NotNull] Discipline discipline)
        {
            discipline.DisciplineId = Guid.NewGuid();
            this._copenhagenByNightEntities.Discipline.Add(discipline);

            this._copenhagenByNightEntities.SaveChanges();

            return discipline;
        }

        [CustomLogAspect]
        [TranslateScopeAspect(MaxRetries = 3)]
        internal Discipline Update([GuidNotNull] Guid disciplineId, [NotNull] Discipline discipline)
        {
            Discipline currentDiscipline =
                this._copenhagenByNightEntities.Discipline.FirstOrDefault(
                    item => item.DisciplineId == disciplineId);

            // ReSharper disable once PossibleNullReferenceException
            currentDiscipline.Name = discipline.Name;
            currentDiscipline.Description = discipline.Description;

            this._copenhagenByNightEntities.SaveChanges();
            return currentDiscipline;
        }

        [CustomLogAspect]
        [TranslateScopeAspect(MaxRetries = 3)]
        internal bool Delete([GuidNotNull] Guid disciplineId)
        {
            Discipline currentDiscipline =
                this._copenhagenByNightEntities.Discipline.FirstOrDefault(
                    item => item.DisciplineId == disciplineId);

            this._copenhagenByNightEntities.Discipline.Remove(currentDiscipline);

            this._copenhagenByNightEntities.SaveChanges();

            return true;
        }
    }
}
