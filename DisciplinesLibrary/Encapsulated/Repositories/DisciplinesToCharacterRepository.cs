﻿namespace DisciplinesLibrary.Encapsulated.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseLibrary;
    using PostSharp.Patterns.Contracts;

    internal class DisciplinesToCharacterRepository
    {
        private readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();


        internal void Add(Guid characterId,Guid disciplineId, [Range(0, 5)] int value)
        {
            this.Update(characterId, disciplineId, value);
        }

        internal void Update(Guid characterId, Guid disciplineId, [Range(0, 5)] int value)
        {
            this._copenhagenByNightEntities.DisciplinesToCharacterInsertOrUdpate(disciplineId, characterId, value);
            this._copenhagenByNightEntities.SaveChanges();
        }

        internal List<DisciplinesToCharacter> Get(Guid characterId)
        {
            return
                this._copenhagenByNightEntities.ProcedureGetDisciplinesForCharacterByCharacterId(characterId)
                    .Select(item => new DisciplinesToCharacter
                    {
                        DisciplineId = item.DisciplineId,
                        CharacterId = characterId,
                        DisciplineLevel = item.DisciplineLevel
                    })
                    .ToList();
        }
    }
}
