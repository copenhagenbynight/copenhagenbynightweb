﻿namespace DisciplinesLibrary.Encapsulated.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AspectLibrary;
    using DatabaseLibrary;
    using PostSharp.Patterns.Contracts;

    internal class DisciplinesRankRepository
    {
        private readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();

        [CustomLogAspect]
        [TranslateScopeAspect(MaxRetries = 3)]
        internal List<DisciplinesRank> Get()
        {
            return this._copenhagenByNightEntities.DisciplinesRank.Select(item => item).ToList();
        }

        [CustomLogAspect]
        [TranslateScopeAspect(MaxRetries = 3)]
        internal DisciplinesRank Get([GuidNotNull] Guid disciplineRankId)
        {
            return
                this._copenhagenByNightEntities.DisciplinesRank.FirstOrDefault(
                    item => item.DisciplinesRankId == disciplineRankId);
        }

        [CustomLogAspect]
        [TranslateScopeAspect(MaxRetries = 3)]
        internal DisciplinesRank Add([NotNull] DisciplinesRank disciplineRank)
        {
            disciplineRank.DisciplineId = Guid.NewGuid();
            this._copenhagenByNightEntities.DisciplinesRank.Add(disciplineRank);

            this._copenhagenByNightEntities.SaveChanges();

            return disciplineRank;
        }

        [CustomLogAspect]
        [TranslateScopeAspect(MaxRetries = 3)]
        internal DisciplinesRank Update([GuidNotNull] Guid disciplineRankId, [NotNull] DisciplinesRank disciplineRank)
        {
            DisciplinesRank currentDiscipline =
                this._copenhagenByNightEntities.DisciplinesRank.FirstOrDefault(
                    item => item.DisciplineId == disciplineRankId);

            // ReSharper disable once PossibleNullReferenceException
            currentDiscipline.Name = disciplineRank.Name;
            currentDiscipline.Description = disciplineRank.Description;
            currentDiscipline.DisciplineId = disciplineRank.DisciplineId;

            this._copenhagenByNightEntities.SaveChanges();
            return currentDiscipline;
        }

        [CustomLogAspect]
        [TranslateScopeAspect(MaxRetries = 3)]
        internal bool Delete([GuidNotNull] Guid disciplineRankId)
        {
            DisciplinesRank currentDisciplineRank =
                this._copenhagenByNightEntities.DisciplinesRank.FirstOrDefault(
                    item => item.DisciplineId == disciplineRankId);

            this._copenhagenByNightEntities.DisciplinesRank.Remove(currentDisciplineRank);

            this._copenhagenByNightEntities.SaveChanges();

            return true;
        }
    }
}
