﻿namespace DisciplinesLibrary.Encapsulated.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseLibrary;

    internal class DisciplineToClanRepository
    {
        private readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();

        internal List<Discipline> GetClanDisciplinesForCharacter(Guid clanId)
        {
            return this._copenhagenByNightEntities.Clan.Where(item => item.ClanId == clanId)
                .Select(item => item.Discipline.ToList())
                .FirstOrDefault();
        }

        internal List<Discipline> GetNoneClanDisciplinesForCharacter(Guid clanId)
        {
            return this._copenhagenByNightEntities.Clan.Where(item => item.ClanId != clanId)
                .Select(item => item.Discipline.ToList())
                .FirstOrDefault();
        }
    }
}
