﻿namespace DisciplinesLibrary.Encapsulated.Managers
{
    using System;
    using System.Collections.Generic;
    using AspectLibrary;
    using Exposed.Models;
    using Factories;
    using PostSharp.Patterns.Contracts;
    using Repositories;

    internal class DisciplineManager
    {
        private readonly DisciplineFactory _factory = new DisciplineFactory();
        private readonly DisciplineRepository _repository = new DisciplineRepository();

        [CustomLogAspect]
        internal List<DisciplineModel> Get()
        {
            return this._factory.Convert(this._repository.Get());
        }

        [CustomLogAspect]
        internal DisciplineModel Get([GuidNotNull] Guid disciplineId)
        {
            return this._factory.Convert(this._repository.Get(disciplineId));
        }

        [CustomLogAspect]
        internal DisciplineModel Add([NotNull] DisciplineAddModel model)
        {
            return this._factory.Convert(this._repository.Add(this._factory.Convert(model)));
        }

        [CustomLogAspect]
        internal DisciplineModel Update([GuidNotNull] Guid disciplineId, [NotNull] DisciplineAddModel model)
        {
            return this._factory.Convert(this._repository.Update(disciplineId, this._factory.Convert(model)));
        }

        [CustomLogAspect]
        internal bool Delete([GuidNotNull] Guid disciplineId)
        {
            return this._repository.Delete(disciplineId);
        }
    }
}
