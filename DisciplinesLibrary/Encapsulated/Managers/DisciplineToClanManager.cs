﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DisciplinesLibrary.Exposed;

namespace DisciplinesLibrary.Encapsulated.Managers
{
    using Factories;
    using Repositories;

    class DisciplineToClanManager
    {
        DisciplineToClanRepository _disciplineToClanRepository = new DisciplineToClanRepository();
        DisciplineToClanFactory _disciplineToClanFactory = new DisciplineToClanFactory();
        internal DisciplinesToClanModel Create(DisciplinesToClanCreateModel model)
        {
            throw new NotImplementedException();
        }

        internal List<DisciplinesToClanModel> Read()
        {
            throw new NotImplementedException();
        }

        internal DisciplinesToClanModel Read(Guid value)
        {
            throw new NotImplementedException();
        }

        internal DisciplinesToClanModel Update(Guid value, DisciplinesToClanUpdateModel model)
        {
            throw new NotImplementedException();
        }

        internal bool Delete(Guid value)
        {
            throw new NotImplementedException();
        }
    }
}
