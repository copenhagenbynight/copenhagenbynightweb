﻿using System;
using System.Collections.Generic;
using DatabaseLibrary;
using DisciplinesLibrary.Encapsulated.Factories;
using DisciplinesLibrary.Encapsulated.Repositories;
using DisciplinesLibrary.Exposed.Models;

namespace DisciplinesLibrary.Encapsulated.Managers
{
    internal class DisciplinesToCharacterManager
    {
        private readonly Guid _characterId;
        private readonly Guid _clanId;

        private readonly DisciplineToClanRepository _disciplineToClanRepository = new DisciplineToClanRepository();
        private readonly DisciplinesToCharacterRepository _disciplinesToCharacterRepository = new DisciplinesToCharacterRepository();
        private readonly DisciplinesToCharacterFactory _disciplinesToCharacterFactory = new DisciplinesToCharacterFactory();

        internal DisciplinesToCharacterManager(Guid characterId, Guid clanId)
        {
            this._characterId = characterId;
            this._clanId = clanId;
        }

        internal CharacterDisciplinesModel Get()
        {


            List<Discipline> listOfClanDisciplines =
                this._disciplineToClanRepository.GetClanDisciplinesForCharacter(this._clanId);

            List<DisciplinesToCharacter> disciplinesToCharacter =
                this._disciplinesToCharacterRepository.Get(this._characterId);

            CharacterDisciplinesModel characterDisciplinesModel = this.SplitBetweenClanAndNoneClan(listOfClanDisciplines, disciplinesToCharacter);
            return characterDisciplinesModel;
        }

        private CharacterDisciplinesModel SplitBetweenClanAndNoneClan(List<Discipline> listOfClanDisciplines, List<DisciplinesToCharacter> disciplinesToCharacter)
        {
            CharacterDisciplinesModel characterDisciplinesModel = new CharacterDisciplinesModel();
            foreach (DisciplinesToCharacter item in disciplinesToCharacter)
            {
                if (this._disciplinesToCharacterFactory.Contains(listOfClanDisciplines, item))
                    characterDisciplinesModel.Clan.Add(this._disciplinesToCharacterFactory.Convert(item));
                else
                    characterDisciplinesModel.NoneClan.Add(this._disciplinesToCharacterFactory.Convert(item));
            }

            return characterDisciplinesModel;
        }

        internal void Update(CharacterDisciplineUpdateModel discipline)
        {
            this._disciplinesToCharacterRepository.Update(this._characterId,discipline.DisciplineId,discipline.Level);
        }

    }
}
