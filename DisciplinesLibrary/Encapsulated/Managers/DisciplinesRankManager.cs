﻿namespace DisciplinesLibrary.Encapsulated.Managers
{
    using System;
    using System.Collections.Generic;
    using AspectLibrary;
    using Exposed;
    using Exposed.Models;
    using Factories;
    using PostSharp.Patterns.Contracts;
    using Repositories;

    internal class DisciplinesRankManager
    {
        private readonly DisciplinesRankFactory _disciplinesRankFactory = new DisciplinesRankFactory();
        private readonly DisciplinesRankRepository _disciplinesRankRepository = new DisciplinesRankRepository();

        internal DisciplinesRankModel Create([NotNull] DisciplinesRankCreateModel model)
        {
            return
                this._disciplinesRankFactory.Convert(
                    this._disciplinesRankRepository.Add(this._disciplinesRankFactory.Convert(model)));
        }

        internal DisciplinesRankModel Read([GuidNotNull] Guid value)
        {
            return this._disciplinesRankFactory.Convert(this._disciplinesRankRepository.Get(value));
        }

        internal List<DisciplinesRankModel> Read()
        {
            return this._disciplinesRankFactory.Convert(this._disciplinesRankRepository.Get());
        }

        internal DisciplinesRankModel Update([GuidNotNull] Guid value,[NotNull] DisciplinesRankUpdateModel model)
        {
            return
                this._disciplinesRankFactory.Convert(this._disciplinesRankRepository.Update(value,
                    this._disciplinesRankFactory.Convert(model)));
        }

        internal bool Delete([GuidNotNull]  Guid value)
        {
            return this._disciplinesRankRepository.Delete(value);
        }
    }
}
