﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DisciplinesLibrary.Exposed.Models
{
    public class DisciplinesRankBaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid DisciplineId { get; set; }
    }
}
