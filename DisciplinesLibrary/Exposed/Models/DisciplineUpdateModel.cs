﻿namespace DisciplinesLibrary.Exposed.Models
{
    using System;
    using AspectLibrary;

    public class DisciplineUpdateModel : DisciplineBaseModel
    {
        [GuidNotNull]
        public Guid DisciplineId { get; set; }
    }
}
