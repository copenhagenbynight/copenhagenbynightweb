﻿namespace DisciplinesLibrary.Exposed.Models
{
    using PostSharp.Patterns.Contracts;

    public class DisciplineBaseModel
    {
        [NotNull]
        public string Description { get; internal set; }
        [StringLength(1, 239)]
        public string Name { get; internal set; }
    }
}