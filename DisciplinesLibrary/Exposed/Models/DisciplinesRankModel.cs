﻿namespace DisciplinesLibrary.Exposed.Models
{
    using System;

    public class DisciplinesRankModel : DisciplinesRankBaseModel
    {
        public Guid DisciplinesRankId { get; set; }
    }
}