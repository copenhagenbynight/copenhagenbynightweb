﻿using System.Collections.Generic;

namespace DisciplinesLibrary.Exposed.Models
{
    public class CharacterDisciplinesModel
    {
        public CharacterDisciplinesModel()
        {
            this.Clan = new List<CharacterDisciplineModel>();
            this.NoneClan = new List<CharacterDisciplineModel>();
        }
        public List<CharacterDisciplineModel> Clan { get; internal set; }
        public List<CharacterDisciplineModel> NoneClan { get; internal set; }
    }
}