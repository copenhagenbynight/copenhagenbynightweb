﻿namespace DisciplinesLibrary.Exposed.Models
{
    using System;
    using AspectLibrary;

    public class DisciplineModel : DisciplineBaseModel
    {
        [GuidNotNull]
        public Guid DisciplineId { get; set; }
    }
}
