﻿using System;

namespace DisciplinesLibrary.Exposed
{
    using Models;
    public class DisciplinesRankUpdateModel : DisciplinesRankBaseModel
    {
        public Guid DisciplinesRankId { get; internal set; }
    }
}