﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DisciplinesLibrary.Exposed.Models
{
    public class CharacterDisciplineUpdateModel
    {
        public Guid DisciplineId { get; internal set; }
        public int Level { get; internal set; }
    }
}
