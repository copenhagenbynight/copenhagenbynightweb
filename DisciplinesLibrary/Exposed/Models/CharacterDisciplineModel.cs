﻿using System;

namespace DisciplinesLibrary.Exposed.Models
{
    public class CharacterDisciplineModel
    {
        public Guid DisciplineId { get; internal set; }
        public string Name { get; internal set; }
        public int Level { get; internal set; }
    }
}