﻿using System;
using DisciplinesLibrary.Encapsulated.Managers;
using DisciplinesLibrary.Exposed.Models;


namespace DisciplinesLibrary.Exposed
{
    public class DisciplinesToCharacterPublisher : IDisciplinesToCharacterPublisher
    {
        readonly DisciplinesToCharacterManager _disciplinesToCharacterManager;
        public DisciplinesToCharacterPublisher(Guid  characterId, Guid clanId)
        {
            this._disciplinesToCharacterManager = new DisciplinesToCharacterManager(characterId, clanId);
        }

        public CharacterDisciplinesModel Get()
        {
           return  this._disciplinesToCharacterManager.Get();
        }

        public void Update(CharacterDisciplineUpdateModel discipline)
        {
            this._disciplinesToCharacterManager.Update(discipline);
        }

    }
}
