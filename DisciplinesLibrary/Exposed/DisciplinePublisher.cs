﻿namespace DisciplinesLibrary.Exposed
{
    using System;
    using System.Collections.Generic;
    using Models;
    using AspectLibrary;
    using Encapsulated.Managers;

    /// <summary>
    /// Disciplines Library - Publisher (Get,Get,Add,Update,Delete)
    /// Using CustomLogAspect, CacheAspect
    /// </summary>
    public class DisciplinePublisher
    {
        readonly DisciplineManager _manager = new DisciplineManager();

        [CustomLogAspect]
        [CacheAspect]
        public List<DisciplineModel> Get()
        {
            return this._manager.Get();
        }

        [CustomLogAspect]
        [CacheAspect]
        public DisciplineModel Get([GuidNotNull] Guid disciplineId)
        {
            return this._manager.Get(disciplineId);
        }

        [CustomLogAspect]
        [CacheAspect]
        public DisciplineModel Add(DisciplineAddModel model)
        {
            return this._manager.Add(model);
        }

        [CustomLogAspect]
        [CacheAspect]
        public DisciplineModel Update([GuidNotNull] Guid disciplineId, DisciplineAddModel model)
        {
            return this._manager.Update(disciplineId,model);
        }

        [CustomLogAspect]
        [CacheAspect]
        public bool Delete([GuidNotNull] Guid disciplineId)
        {
            return this._manager.Delete(disciplineId);
        }
    }
}
