﻿namespace DisciplinesLibrary.Exposed
{
    using System;
    using System.Collections.Generic;
    using AspectLibrary;
    using Encapsulated.Managers;

    public class DisciplinesToClanPublisher
    {
        private readonly DisciplineToClanManager _disciplinesToClanManager = new DisciplineToClanManager();


        [CustomLogAspect]
        [CacheAspect]
        public DisciplinesToClanModel Create(DisciplinesToClanCreateModel model)
        {
            return this._disciplinesToClanManager.Create(model);
        }

        [CustomLogAspect]
        [CacheAspect]
        public List<DisciplinesToClanModel> Read()
        {
            return this._disciplinesToClanManager.Read();
        }

        [CustomLogAspect]
        [CacheAspect]
        public DisciplinesToClanModel Read(Guid value)
        {
            return this._disciplinesToClanManager.Read(value);
        }

        [CustomLogAspect]
        [CacheAspect]
        public DisciplinesToClanModel Update(Guid value, DisciplinesToClanUpdateModel model)
        {
            return this._disciplinesToClanManager.Update(value, model);
        }

        [CustomLogAspect]
        [CacheAspect]
        public bool Delete(Guid value)
        {
            return this._disciplinesToClanManager.Delete(value);
        }
    }
}
