﻿namespace DisciplinesLibrary.Exposed
{
    using System;
    using System.Collections.Generic;
    using AspectLibrary;
    using Encapsulated.Managers;
    using Models;

    public class DisciplinesRankPublisher
    {
        private readonly DisciplinesRankManager _disciplinesRankManager = new DisciplinesRankManager();

        [CustomLogAspect]
        [CacheAspect]
        public DisciplinesRankModel Create(DisciplinesRankCreateModel model)
        {
            return this._disciplinesRankManager.Create(model);
        }

        [CustomLogAspect]
        [CacheAspect]
        public List<DisciplinesRankModel> Read()
        {
            return this._disciplinesRankManager.Read();
        }

        [CustomLogAspect]
        [CacheAspect]
        public DisciplinesRankModel Read(Guid value)
        {
            return this._disciplinesRankManager.Read(value);
        }

        [CustomLogAspect]
        [CacheAspect]
        public DisciplinesRankModel Update(Guid value, DisciplinesRankUpdateModel model)
        {
            return this._disciplinesRankManager.Update(value, model);
        }

        [CustomLogAspect]
        [CacheAspect]
        public bool Delete(Guid value)
        {
            return this._disciplinesRankManager.Delete(value);
        }
    }
}
