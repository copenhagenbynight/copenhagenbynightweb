﻿namespace DisciplinesLibrary
{
    using Exposed.Models;

    public interface IDisciplinesToCharacterPublisher
    {
        CharacterDisciplinesModel Get();
        void Update(CharacterDisciplineUpdateModel discipline);
    }
}