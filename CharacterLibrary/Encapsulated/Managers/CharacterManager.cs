﻿namespace CharacterLibrary.Encapsulated.Managers
{
    using System;
    using BloodPotencyLibrary;
    using BloodPotencyLibrary.Exposed;
    using BloodPotencyLibrary.Exposed.Implementation;
    using ClanLibrary;
    using ClanLibrary.Exposed.Publishers;
    using ClientsLibrary;
    using ClientsLibrary.Exposed;
    using CovenantsLibrary;
    using CovenantsLibrary.Exposed;
    using DisciplinesLibrary;
    using Exposed.Models;
    using Factories;
    using HumanityLibrary;
    using HumanityLibrary.Exposed.Publishers;
    using Repositories;
    using DisciplinesLibrary.Exposed;

    internal class CharacterManager
    {
        private readonly IClanToCharacterPublisher _clanToCharacterPublisher;
        private readonly IBloodPotencyToCharacterPublisher _bloodPotencyToCharacterPublisher;
        private readonly IClientsToCharacterPublisher _clientsToCharacterPublisher;
        private readonly ICovenantToCharacterPublisher _covenantToCharacterPublisher;
        private readonly IHumanityToCharacterPublisher _humanityToCharacterPublisher;
        private readonly IDisciplinesToCharacterPublisher _disciplinesToCharacterPublisher;
        private readonly CharacterRepository _characterRepository = new CharacterRepository();
        private readonly CharacterFactory _characterFactory = new CharacterFactory();
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private Guid _characterId;

        internal CharacterManager(Guid characterId)
        {
            this._clanToCharacterPublisher = new ClanToCharacterPublisher(characterId);
            this._bloodPotencyToCharacterPublisher = new BloodPotencyToCharacterPublisher(characterId);
            this._clientsToCharacterPublisher = new ClientsToCharacterPublisher(characterId);
            this._covenantToCharacterPublisher = new CovenantToCharacterPublisher(characterId);
            this._humanityToCharacterPublisher = new HumanityToCharacterPublisher(characterId);
            this._disciplinesToCharacterPublisher = new DisciplinesToCharacterPublisher(characterId, this._clanToCharacterPublisher.Get().ClanId);
            this._characterId = characterId;
        }

        internal void Create(CharacterAddModel model)
        {
            this._characterRepository.Add(this._characterFactory.Convert(model));

            this._bloodPotencyToCharacterPublisher.SetDefault();
            this._clanToCharacterPublisher.SetDefault();
            this._clientsToCharacterPublisher.SetDefault();
            this._covenantToCharacterPublisher.SetDefault();
            this._humanityToCharacterPublisher.SetDefault();
        }

        public CharacterModel GetOne()
        {
            CharacterModel character  = this._characterFactory.Convert(this._characterRepository.Get(this._characterId));

            character.Clan = this._clanToCharacterPublisher.Get();
            character.BloodPotency = this._bloodPotencyToCharacterPublisher.Get();
            character.Clients = this._clientsToCharacterPublisher.Get();
            character.Covenant = this._covenantToCharacterPublisher.Get();
            character.Humanity = this._humanityToCharacterPublisher.Get();
            character.Disciplines = this._disciplinesToCharacterPublisher.Get();

            return character;
        }
    }
}
