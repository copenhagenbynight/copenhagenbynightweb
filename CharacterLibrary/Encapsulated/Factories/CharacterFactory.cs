﻿namespace CharacterLibrary.Encapsulated.Factories
{
    using DatabaseLibrary;
    using Exposed.Models;

    internal class CharacterFactory
    {
        public Character Convert(CharacterAddModel model)
        {
            return new Character
            {
                Name = model.Name,
                DateOfEmbrace = model.DateOfEmbrace,
                BackgroundExperiencePoints = 0,
                BaseExperiencePoints = 6,
                BaseCombatFigures = 1,
                UserId = model.UserId
            };
        }

        public Character Convert(CharacterUpdateModel model)
        {
            return new Character
            {
                Name = model.Name,
                DateOfEmbrace = model.DateOfEmbrace,
                BackgroundExperiencePoints = model.BackgroundExperiencePoints,
                BaseExperiencePoints = model.BaseExperiencePoints,
                BaseCombatFigures = model.BaseCombatFigures,
                UserId = model.UserId,
                CharacterId = model.CharacterId
            };
        }

        public Character Convert(CharacterModel model)
        {
            return new Character
            {
                Name = model.Name,
                DateOfEmbrace = model.DateOfEmbrace,
                BackgroundExperiencePoints = model.BackgroundExperiencePoints,
                BaseExperiencePoints = model.BaseExperiencePoints,
                BaseCombatFigures = model.BaseCombatFigures,
                UserId = model.UserId,
                CharacterId = model.CharacterId
            };
        }

        public CharacterModel Convert(Character model)
        {
            return new CharacterModel
            {
                Name = model.Name,
                DateOfEmbrace = model.DateOfEmbrace,
                BackgroundExperiencePoints = model.BackgroundExperiencePoints,
                BaseExperiencePoints = model.BaseExperiencePoints,
                BaseCombatFigures = model.BaseCombatFigures,
                UserId = model.UserId,
                CharacterId = model.CharacterId
            };
        }
    }
}
