﻿namespace CharacterLibrary.Encapsulated.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AspectLibrary;
    using DatabaseLibrary;
    using PostSharp.Patterns.Contracts;

    internal class CharacterRepository
    {
        private readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();

        internal Character Get([GuidNotNull] Guid characterId)
        {
            return
                this._copenhagenByNightEntities.Character.FirstOrDefault(
                    item => item.CharacterId == characterId);
        }

        internal List<Character> Get()
        {
            return
                this._copenhagenByNightEntities.Character.OrderBy(item => item.Name)
                    .Select(item => item)
                    .ToList();
        }

        public Character Update([GuidNotNull] Guid characterId, [NotNull] Character model)
        {
            Character currentCharacter =
                this._copenhagenByNightEntities.Character.FirstOrDefault(
                    item => item.CharacterId == characterId);

            // ReSharper disable once PossibleNullReferenceException
            currentCharacter.Name = model.Name;
            currentCharacter.BackgroundExperiencePoints = model.BackgroundExperiencePoints;
            currentCharacter.BaseExperiencePoints = model.BaseExperiencePoints;
            currentCharacter.BaseCombatFigures = model.BaseCombatFigures;
            currentCharacter.CharacterId = model.CharacterId;
            currentCharacter.DateOfEmbrace = model.DateOfEmbrace;
            currentCharacter.UserId = model.UserId;

            this._copenhagenByNightEntities.SaveChanges();
            return currentCharacter;
        }

        internal bool Delete([GuidNotNull] Guid characterId)
        {
            Character currentCharacter =
                this._copenhagenByNightEntities.Character.FirstOrDefault(
                    item => item.CharacterId == characterId);

            this._copenhagenByNightEntities.Character.Remove(currentCharacter);

            this._copenhagenByNightEntities.SaveChanges();

            return true;
        }

        internal Character Add(Character character)
        {
            character.CharacterId = Guid.NewGuid();
            this._copenhagenByNightEntities.Character.Add(character);

            this._copenhagenByNightEntities.SaveChanges();

            return character;
        }
    }
}
