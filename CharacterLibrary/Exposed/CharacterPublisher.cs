﻿namespace CharacterLibrary.Exposed
{
    using System;
    using Encapsulated.Managers;
    using Models;

    public class CharacterPublisher
    {

        public CharacterModel Get(Guid characterId)
        {
            CharacterManager characterManager =  new CharacterManager(characterId);
            return characterManager.GetOne();
        }

        public CharacterModel Add(CharacterAddModel character)
        {
            CharacterManager characterManager = new CharacterManager(Guid.NewGuid());
            characterManager.Create(character);
            return characterManager.GetOne();
        }
    }
}

