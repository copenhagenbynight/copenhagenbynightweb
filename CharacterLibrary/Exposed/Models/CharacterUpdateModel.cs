﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterLibrary.Exposed.Models
{
    public class CharacterUpdateModel
    {
        public int BackgroundExperiencePoints { get; internal set; }
        public int BaseCombatFigures { get; internal set; }
        public int BaseExperiencePoints { get; internal set; }
        public Guid CharacterId { get; internal set; }
        public DateTime DateOfEmbrace { get; internal set; }
        public string Name { get; internal set; }
        public string UserId { get; internal set; }
    }
}
