﻿using ClanLibrary.Exposed.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BloodPotencyLibrary.Exposed.Models;

namespace CharacterLibrary.Exposed.Models
{
    using ClientsLibrary.Exposed.Models;
    using CovenantsLibrary.Exposed.Models;
    using DisciplinesLibrary.Exposed.Models;
    using HumanityLibrary.Exposed.Models;

    public class CharacterModel
    {
        public int BackgroundExperiencePoints { get; internal set; }
        public int BaseCombatFigures { get; internal set; }
        public int BaseExperiencePoints { get; internal set; }
        public BloodPotencyModel BloodPotency { get; internal set; }
        public Guid CharacterId { get; set; }
        public ClanModel Clan { get; set; }
        public ClientsModel Clients { get; internal set; }
        public CovenantsModel Covenant { get; internal set; }
        public DateTime DateOfEmbrace { get; internal set; }
        public CharacterDisciplinesModel Disciplines { get; internal set; }
        public HumanityModel Humanity { get; internal set; }
        public string Name { get; internal set; }
        public string UserId { get; internal set; }
    }
}
