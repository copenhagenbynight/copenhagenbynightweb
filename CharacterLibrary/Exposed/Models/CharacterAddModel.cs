﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterLibrary.Exposed.Models
{
    public class CharacterAddModel
    {
        public DateTime DateOfEmbrace { get; internal set; }
        public string Name { get; internal set; }
        public string UserId { get; internal set; }
    }
}
