﻿namespace ClientsLibrary
{
    using System;
    using System.Collections.Generic;
    using Exposed.Models;
    using PostSharp.Patterns.Contracts;
    using AspectLibrary;

    public interface IClientsPublisher
    {
        ClientsModel Add([NotNull] ClientsAddModel model);
        bool Delete([GuidNotNull] Guid clientId);
        List<ClientsModel> Get();
        ClientsModel Get([GuidNotNull] Guid clientId);
        ClientsModel Update([GuidNotNull] Guid clientId, [NotNull] ClientsUpdateModel model);
    }
}