﻿namespace ClientsLibrary
{
    using System;
    using AspectLibrary;
    using Exposed.Models;

    public interface IClientsToCharacterPublisher
    {
        ClientsModel Get();
        ClientsModel Update([GuidNotNull] Guid clanId);
        bool SetDefault();
    }
}