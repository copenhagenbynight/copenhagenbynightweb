﻿namespace ClientsLibrary.Encapsulated.Managers
{
    using System;
    using System.Linq;
    using AspectLibrary;
    using Exposed.Models;
    using Factories;
    using Repositories;

    internal class ClientsToCharacterManager
    {
        private readonly ClientsRepository _clientsRepository = new ClientsRepository();
        private readonly ClientsToCharacterRepository _clientsToCharacterRepository = new ClientsToCharacterRepository();
        private readonly ClientsFactory _clientsFactory = new ClientsFactory();

        internal ClientsModel Get([GuidNotNull] Guid characterId)
        {
            return this._clientsFactory.Convert(this._clientsToCharacterRepository.Get(characterId));
        }


        internal ClientsModel Update([GuidNotNull] Guid characterId, [GuidNotNull] Guid clientsId)
        {
            return this._clientsFactory.Convert(this._clientsToCharacterRepository.Set(characterId, clientsId));
        }

        internal bool SetDefault([GuidNotNull] Guid characterId)
        {
            // ReSharper disable once PossibleNullReferenceException
            this.Update(characterId, this._clientsRepository.Get().FirstOrDefault().ClientId);
            return true;
        }
    }
}
