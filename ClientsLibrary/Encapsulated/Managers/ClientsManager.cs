﻿namespace ClientsLibrary.Encapsulated.Managers
{
    using System;
    using System.Collections.Generic;
    using AspectLibrary;
    using Exposed.Models;
    using Factories;
    using PostSharp.Patterns.Contracts;
    using Repositories;

    internal class ClientsManager
    {
        private readonly ClientsRepository _clientRepository = new ClientsRepository();
        private readonly ClientsFactory _clientConvertFactory = new ClientsFactory();

        internal List<ClientsModel> Get()
        {
            return this._clientConvertFactory.Convert(this._clientRepository.Get());
        }

        internal ClientsModel Get([GuidNotNull] Guid clientId)
        {
            return this._clientConvertFactory.Convert(this._clientRepository.Get(clientId));
        }

        internal ClientsModel Add([NotNull] ClientsAddModel model)
        {
            return this._clientConvertFactory.Convert(this._clientRepository.Add(this._clientConvertFactory.Convert(model)));
        }

        internal ClientsModel Update([GuidNotNull] Guid clientId, [NotNull] ClientsUpdateModel model)
        {
            return
                this._clientConvertFactory.Convert(this._clientRepository.Update(clientId,
                    this._clientConvertFactory.Convert(model)));
        }

        internal bool Delete([GuidNotNull] Guid value)
        {
            return this._clientRepository.Delete(value);
        }
    }
}
