﻿namespace ClientsLibrary.Encapsulated.Repositories
{
    using System;
    using System.Collections.Generic;
    using DatabaseLibrary;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class ClientsToCharacter
    {
        [TestMethod]
        public void SetTestMethod()
        {
            ClientsToCharacterRepository repository = new ClientsToCharacterRepository();

            Guid x = Guid.Parse("6fc08e27-44ed-46c4-ad82-110772ab76f8");
            Guid y = Guid.Parse("36F94AFE-A96A-4DC4-ADD5-29F530A230CB");

            repository.Set(x, y);
            
        }

        [TestMethod]
        public void GetTestMethod()
        {
            ClientsRepository clientsRepository = new ClientsRepository();

            Client input = new Client
            {

                Name = "Test Clients",
                Description = "Test Clients",
                ClientRank = 99,
                NumberOf = 99



            };
            // ReSharper disable once UnusedVariable
            Client resultAdd = clientsRepository.Add(input);

            Assert.AreEqual(resultAdd.Name, input.Name);
            Assert.AreEqual(resultAdd.Description, input.Description);

            Client resultGet = clientsRepository.Get(resultAdd.ClientId);

        }
    }
}
