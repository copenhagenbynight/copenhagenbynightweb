﻿namespace ClientsLibrary.Encapsulated.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AspectLibrary;
    using DatabaseLibrary;
    using PostSharp.Patterns.Contracts;

    internal class ClientsRepository
    {
        private readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();

        [TranslateScopeAspect(MaxRetries = 3)]
        internal List<Client> Get()
        {
            return this._copenhagenByNightEntities.Client.OrderBy(item => item.Name).Select(item => item).OrderBy(item => item.ClientRank).ToList();
        }

        [TranslateScopeAspect(MaxRetries = 3)]
        internal Client Get([GuidNotNull] Guid clientId)
        {
            return this._copenhagenByNightEntities.Client.FirstOrDefault(item => item.ClientId == clientId);
        }

        [TranslateScopeAspect(MaxRetries = 3)]
        internal Client Add([NotNull] Client value)
        {
            value.ClientId = Guid.NewGuid();
            this._copenhagenByNightEntities.Client.Add(value);
            this._copenhagenByNightEntities.SaveChanges();
            return value;
        }

        [TranslateScopeAspect(MaxRetries = 3)]
        internal Client Update([GuidNotNull] Guid clientId, [NotNull] Client value)
        {
            Client currentClient = new Client {ClientId = clientId};
            this._copenhagenByNightEntities.Client.Attach(currentClient);

            currentClient.Description = value.Description;
            currentClient.Name = value.Name;

            this._copenhagenByNightEntities.SaveChanges();
            return value;
        }

        [TranslateScopeAspect(MaxRetries = 3)]
        internal bool Delete([GuidNotNull] Guid clientId)
        {
            Client currentClient = new Client {ClientId = clientId};
            this._copenhagenByNightEntities.Client.Attach(currentClient);
            this._copenhagenByNightEntities.Client.Remove(currentClient);
            this._copenhagenByNightEntities.SaveChanges();
            return true;
        }
    }
}

