﻿namespace ClientsLibrary.Encapsulated.Repositories
{
    using System;
    using System.Linq;
    using AspectLibrary;
    using DatabaseLibrary;

    internal class ClientsToCharacterRepository
    {
        private readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();

        [TranslateScopeAspect(MaxRetries = 3)]
        internal Client Set([GuidNotNull] Guid characterId, [GuidNotNull] Guid clientsId)
        {
            Character currentCharacter =
                this._copenhagenByNightEntities.Character.FirstOrDefault(item => item.CharacterId == characterId);


            Client currentClient =
                this._copenhagenByNightEntities.Client.FirstOrDefault(item => item.ClientId == clientsId);

            // ReSharper disable PossibleNullReferenceException
            currentCharacter.ClientToCharacter.ClientId = currentClient.ClientId;
            // ReSharper restore PossibleNullReferenceException
            this._copenhagenByNightEntities.SaveChanges();
            return currentClient;
        }

        [TranslateScopeAspect(MaxRetries = 3)]
        internal Client Get(Guid characterId)
        {
            return this._copenhagenByNightEntities.Character.Where(item => item.CharacterId == characterId)
                .Select(item => item.ClientToCharacter.Client).FirstOrDefault();
        }
    }
}
