﻿namespace ClientsLibrary.Encapsulated.Factories
{
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseLibrary;
    using Exposed.Models;
    using PostSharp.Patterns.Contracts;

    internal class ClientsFactory
    {
        internal ClientsModel Convert([NotNull] Client client)
        {
            return new ClientsModel
            {
                ClientId = client.ClientId,
                Description = client.Description,
                Name = client.Name,
                ClientRank = client.ClientRank,
                NumberOf = client.NumberOf
            };
        }

        internal Client Convert([NotNull] ClientsAddModel client)
        {
            return new Client
            {
                Description = client.Description,
                Name = client.Name,
                ClientRank = client.ClientRank,
                NumberOf = client.NumberOf
            };
        }

        internal Client Convert([NotNull] ClientsUpdateModel client)
        {
            return new Client
            {
                ClientId = client.ClientId,
                Description = client.Description,
                Name = client.Name,
                ClientRank = client.ClientRank,
                NumberOf = client.NumberOf
            };
        }

        internal List<ClientsModel> Convert([NotNull] List<Client> list)
        {
            return (list.Select(this.Convert)).ToList();
        }
    }
}
