﻿using System;
using ClientsLibrary.Exposed.Models;

namespace ClientsLibrary.Exposed
{
    using AspectLibrary;
    using Encapsulated.Managers;

    public class ClientsToCharacterPublisher : IClientsToCharacterPublisher
    {
        private readonly Guid _characterId;
        readonly ClientsToCharacterManager _clientsToCharacterManager = new ClientsToCharacterManager();
        public ClientsToCharacterPublisher([GuidNotNull] Guid characterId)
        {
            this._characterId = characterId;
        }
       
        public ClientsModel Get()
        {
            return this.Get(this._characterId);
        }


        public ClientsModel Update([GuidNotNull] Guid clientsId)
        {
            return this.Update(this._characterId, clientsId);
        }

    

        public bool SetDefault()
        {
            return this.SetDefault(this._characterId);
        }


        [CacheAspect]
        private ClientsModel Update([GuidNotNull] Guid characterId, [GuidNotNull] Guid clientsId)
        {
            return this._clientsToCharacterManager.Update(characterId, clientsId);
        }

        [CacheAspect]
        private ClientsModel Get([GuidNotNull] Guid characterId)
        {
            return this._clientsToCharacterManager.Get(characterId);
        }

        [CacheAspect]
        private bool SetDefault([GuidNotNull] Guid clientsId)
        {
            return this._clientsToCharacterManager.SetDefault(clientsId);
        }
    }
}