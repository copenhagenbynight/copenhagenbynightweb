﻿namespace ClientsLibrary.Exposed
{
    using System;
    using System.Collections.Generic;
    using AspectLibrary;
    using Encapsulated.Managers;
    using Models;
    using PostSharp.Patterns.Contracts;

    public class ClientsPublisher : IClientsPublisher
    {
        private readonly ClientsManager _clientsManager = new ClientsManager();

        [CacheAspect]
        public List<ClientsModel> Get()
        {
            return this._clientsManager.Get();
        }

        [CacheAspect]
        public ClientsModel Get([GuidNotNull] Guid clientId)
        {
            return this._clientsManager.Get(clientId);
        }

        [CacheAspect]
        public ClientsModel Add([NotNull] ClientsAddModel model)
        {
            return this._clientsManager.Add(model);
        }

        [CacheAspect]
        public ClientsModel Update([GuidNotNull] Guid clientId, [NotNull] ClientsUpdateModel model)
        {
            return this._clientsManager.Update(clientId, model);
        }

        [CacheAspect]
        public bool Delete([GuidNotNull] Guid clientId)
        {
            return this._clientsManager.Delete(clientId);
        }
    }
}
