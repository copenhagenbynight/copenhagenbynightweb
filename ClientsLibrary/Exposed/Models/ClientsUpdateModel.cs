﻿namespace ClientsLibrary.Exposed.Models
{
    using System;
    using AspectLibrary;

    public class ClientsUpdateModel : ClientsBaseModel
    {
        [GuidNotNull]
        public Guid ClientId { get; internal set; }
    }
}