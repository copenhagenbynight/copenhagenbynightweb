﻿namespace ClientsLibrary.Exposed.Models
{
    using System;
    using AspectLibrary;

    public class ClientsToCharacterSetModel
    {
        [GuidNotNull]
        public Guid ClientId { get; set; }
    }
}