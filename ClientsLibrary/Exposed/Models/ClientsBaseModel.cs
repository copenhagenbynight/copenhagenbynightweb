﻿namespace ClientsLibrary.Exposed.Models
{
    using PostSharp.Patterns.Contracts;

    public class ClientsBaseModel
    {
        [Range(0, 5)]
        public int ClientRank { get; set; }

        [StringLength(1, 9999)]
        public string Description { get; set; }

        [StringLength(1, 239)]
        public string Name { get; set; }

        [StrictlyPositive]
        public int NumberOf { get; set; }
    }
}