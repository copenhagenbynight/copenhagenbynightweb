﻿namespace ClientsLibrary.Exposed.Models
{
    using System;
    using AspectLibrary;

    public class ClientsModel : ClientsBaseModel
    {
        [GuidNotNull]
        public Guid ClientId { get; internal set; }
    }
}