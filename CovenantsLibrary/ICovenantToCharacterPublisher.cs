﻿namespace CovenantsLibrary
{
    using System;
    using AspectLibrary;
    using Exposed.Models;

    public interface ICovenantToCharacterPublisher
    {
        CovenantsModel Get();
        CovenantsModel Update([GuidNotNull] Guid clientsId);
        bool SetDefault();
    }
}