﻿namespace CovenantsLibrary
{
    using System;
    using System.Collections.Generic;
    using AspectLibrary;
    using Exposed.Models;
    using PostSharp.Patterns.Contracts;

    public interface ICovenantsPublisher
    {
        CovenantsModel Add([NotNull] CovenantsAddModel model);
        bool Delete([GuidNotNull] Guid value);
        List<CovenantsModel> Get();
        CovenantsModel Get([GuidNotNull] Guid value);
        CovenantsModel Update([GuidNotNull] Guid value, [NotNull] CovenantsUpdateModel model);
    }
}