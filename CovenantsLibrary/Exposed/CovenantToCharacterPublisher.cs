﻿namespace CovenantsLibrary.Exposed
{
    using System;
    using AspectLibrary;
    using Encapsulated.Managers;
    using Models;

    public class CovenantToCharacterPublisher : ICovenantToCharacterPublisher
    {
        private readonly Guid _characterId;
        readonly CovenantToCharacterManager _covenantToCharacterManager = new CovenantToCharacterManager();

        public CovenantToCharacterPublisher([GuidNotNull] Guid characterId)
        {
            this._characterId = characterId;
        }

        public CovenantsModel Get()
        {
            return this.Get(this._characterId);
        }

        public CovenantsModel Update([GuidNotNull] Guid covenantId)
        {
            return this.Update(this._characterId, covenantId);
        }

        public bool SetDefault()
        {
            return this.SetDefault(this._characterId);
        }


        [CacheAspect]
        private CovenantsModel Get([GuidNotNull] Guid characterId)
        {
            return this._covenantToCharacterManager.Get(characterId);
        }

        [CacheAspect]
        private CovenantsModel Update([GuidNotNull] Guid characterId,[GuidNotNull] Guid covenantId)
        {
            return this._covenantToCharacterManager.Set(characterId, covenantId);
        }

        [CacheAspect]
        private bool SetDefault([GuidNotNull] Guid characterId)
        {
            return this._covenantToCharacterManager.SetDefault(characterId);
        }
    }
}