﻿namespace CovenantsLibrary.Exposed.Models
{
    using System;
    using AspectLibrary;
    using PostSharp.Patterns.Contracts;

    public class CovenantsModel : CovenantBaseModel
    {
        [GuidNotNull]
        public Guid CovenantId { get; internal set; }
    }
}