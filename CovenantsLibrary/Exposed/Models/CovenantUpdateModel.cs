﻿namespace CovenantsLibrary.Exposed.Models
{
    using System;
    using AspectLibrary;

    public class CovenantsUpdateModel : CovenantBaseModel
    {
        [GuidNotNull]
        public Guid CovenantId { get; internal set; }
    }
}