﻿namespace CovenantsLibrary.Exposed.Models
{
    using PostSharp.Patterns.Contracts;

    public class CovenantBaseModel
    {
        [NotNull]
        public string Description { get; internal set; }
        [StringLength(1,239)]
        public string Name { get; internal set; }
    }
}