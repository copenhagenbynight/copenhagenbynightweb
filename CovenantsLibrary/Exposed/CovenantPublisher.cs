﻿using System;
using System.Collections.Generic;
using CovenantsLibrary.Encapsulated.Managers;
using CovenantsLibrary.Exposed.Models;
using PostSharp.Patterns.Contracts;
using AspectLibrary;

namespace CovenantsLibrary.Exposed
{
    public class CovenantsPublisher : ICovenantsPublisher
    {
        private readonly CovenantsManager _covenantsmanager = new CovenantsManager();

        [CacheAspect]
        public List<CovenantsModel> Get()
        {
            return this._covenantsmanager.Get();
        }

        [CacheAspect]
        public CovenantsModel Get([GuidNotNull] Guid value)
        {
            return this._covenantsmanager.Get(value);
        }

        [CacheAspect]
        public CovenantsModel Add([NotNull] CovenantsAddModel model)
        {
            return this._covenantsmanager.Add(model);
        }

        [CacheAspect]
        public CovenantsModel Update([GuidNotNull] Guid value, [NotNull] CovenantsUpdateModel model)
        {
            return this._covenantsmanager.Update(value, model);
        }

        [CacheAspect]
        public bool Delete([GuidNotNull] Guid value)
        {
            return this._covenantsmanager.Delete(value);
        }
    }
}
