﻿using System.Collections.Generic;
using System.Linq;
using CovenantsLibrary.Exposed.Models;
using DatabaseLibrary;
using PostSharp.Patterns.Contracts;

namespace CovenantsLibrary.Encapsulated.Factories
{
    internal class CovenantsFactory
    {
        internal CovenantsModel Convert([NotNull] Covenant covenant)
        {
            return new CovenantsModel
            {
                CovenantId = covenant.CovenantId,
                Description = covenant.Description,
                Name = covenant.Name,
            };
        }

        internal Covenant Convert([NotNull] CovenantsAddModel covenant)
        {
            return new Covenant
            {
                Description = covenant.Description,
                Name = covenant.Name,

            };
        }

        internal Covenant Convert([NotNull] CovenantsUpdateModel covenant)
        {
            return new Covenant
            {
                CovenantId = covenant.CovenantId,
                Description = covenant.Description,
                Name = covenant.Name
            };
        }

        internal List<CovenantsModel> Convert([NotNull] List<Covenant> list)
        {
            return (list.Select(this.Convert)).ToList();
        }
    }
}
