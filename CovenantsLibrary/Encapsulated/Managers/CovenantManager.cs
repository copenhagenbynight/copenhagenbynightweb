﻿namespace CovenantsLibrary.Encapsulated.Managers
{
    using System;
    using System.Collections.Generic;
    using AspectLibrary;
    using Exposed.Models;
    using Factories;
    using PostSharp.Patterns.Contracts;
    using Repositories;

    internal class CovenantsManager
    {
        private readonly CovenantsRepository _covenantRepository = new CovenantsRepository();
        private readonly CovenantsFactory _covenantConvertFactory = new CovenantsFactory();

        internal List<CovenantsModel> Get()
        {
            return this._covenantConvertFactory.Convert(this._covenantRepository.Get());
        }

        internal CovenantsModel Get([GuidNotNull] Guid covenantId)
        {
            return this._covenantConvertFactory.Convert(this._covenantRepository.Get(covenantId));
        }

        internal CovenantsModel Add([NotNull] CovenantsAddModel model)
        {
            return
                this._covenantConvertFactory.Convert(
                    this._covenantRepository.Add(this._covenantConvertFactory.Convert(model)));
        }

        internal CovenantsModel Update([GuidNotNull] Guid covenantId, [NotNull] CovenantsUpdateModel model)
        {
            return
                this._covenantConvertFactory.Convert(this._covenantRepository.Update(covenantId,
                    this._covenantConvertFactory.Convert(model)));
        }

        internal bool Delete([GuidNotNull] Guid value)
        {
            return this._covenantRepository.Delete(value);
        }
    }
}
