﻿namespace CovenantsLibrary.Encapsulated.Managers
{
    using System;
    using System.Linq;
    using AspectLibrary;
    using Repositories;
    using Exposed.Models;
    using Factories;

    internal class CovenantToCharacterManager
    {
        private readonly CovenantsRepository _repository = new CovenantsRepository();
        private readonly CovenantsToCharacterRepository _characterRepository = new CovenantsToCharacterRepository();
        private readonly CovenantsFactory _clientsFactory = new CovenantsFactory();

        internal CovenantsModel Get([GuidNotNull] Guid characterId)
        {
            return this._clientsFactory.Convert(this._characterRepository.Get(characterId));
        }

        internal CovenantsModel Set([GuidNotNull] Guid characterId, [GuidNotNull]  Guid clientsId)
        {
            return this._clientsFactory.Convert(this._characterRepository.Set(characterId, clientsId));
        }

        internal bool SetDefault([GuidNotNull] Guid characterId)
        {
            // ReSharper disable once PossibleNullReferenceException
            this.Set(characterId, this._repository.Get().FirstOrDefault().CovenantId);
            return true;
        }
    }
}
