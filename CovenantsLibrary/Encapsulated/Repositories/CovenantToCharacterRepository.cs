﻿namespace CovenantsLibrary.Encapsulated.Repositories
{
    using System;
    using System.Linq;
    using AspectLibrary;
    using DatabaseLibrary;

    internal class CovenantsToCharacterRepository
    {
        private readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();

       
        [TranslateScopeAspect(MaxRetries = 3)]
        internal Covenant Set([GuidNotNull] Guid characterId, Guid covenantsId)
        {
            Character currentCharacter =
                this._copenhagenByNightEntities.Character.FirstOrDefault(item => item.CharacterId == characterId);


            Covenant currentCovenant =
                this._copenhagenByNightEntities.Covenant.FirstOrDefault(item => item.CovenantId == covenantsId);

            // ReSharper disable PossibleNullReferenceException
            currentCharacter.CovenentId = currentCovenant.CovenantId;
            // ReSharper restore PossibleNullReferenceException
            this._copenhagenByNightEntities.SaveChanges();
            return currentCovenant;
        }

        [TranslateScopeAspect(MaxRetries = 3)]
        internal Covenant Get([GuidNotNull] Guid characterId)
        {
            return this._copenhagenByNightEntities.Character.Where(item => item.CharacterId == characterId)
                .Select(item => item.Covenant).FirstOrDefault();
        }
    }
}
