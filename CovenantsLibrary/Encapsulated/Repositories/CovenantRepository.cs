﻿namespace CovenantsLibrary.Encapsulated.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AspectLibrary;
    using DatabaseLibrary;
    using PostSharp.Patterns.Contracts;

    internal class CovenantsRepository
    {
        private readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();

        [TranslateScopeAspect(MaxRetries = 3)]
        internal List<Covenant> Get()
        {
            return
                this._copenhagenByNightEntities.Covenant.OrderBy(item => item.Name)
                    .Select(item => item)
                    .OrderBy(item => item.Name)
                    .ToList();
        }

        [TranslateScopeAspect(MaxRetries = 3)]
        internal Covenant Get([GuidNotNull] Guid covenantId)
        {
            return this._copenhagenByNightEntities.Covenant.FirstOrDefault(item => item.CovenantId == covenantId);
        }

        [TranslateScopeAspect(MaxRetries = 3)]
        internal Covenant Add([NotNull] Covenant value)
        {
            this._copenhagenByNightEntities.Covenant.Add(value);
            this._copenhagenByNightEntities.SaveChanges();
            return value;
        }

        [TranslateScopeAspect(MaxRetries = 3)]
        internal Covenant Update([GuidNotNull] Guid covenantId, [NotNull] Covenant value)
        {
            Covenant currentCovenant = new Covenant {CovenantId = covenantId};
            this._copenhagenByNightEntities.Covenant.Attach(currentCovenant);

            currentCovenant.Description = value.Description;
            currentCovenant.Name = value.Name;

            this._copenhagenByNightEntities.SaveChanges();
            return value;
        }

        [TranslateScopeAspect(MaxRetries = 3)]
        internal bool Delete([GuidNotNull] Guid covenantId)
        {
            Covenant currentCovenant = new Covenant {CovenantId = covenantId};
            this._copenhagenByNightEntities.Covenant.Attach(currentCovenant);
            this._copenhagenByNightEntities.Covenant.Remove(currentCovenant);
            this._copenhagenByNightEntities.SaveChanges();
            return true;
        }
    }
}