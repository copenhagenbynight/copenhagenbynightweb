﻿using System;

namespace AccountAssignUsersRolesLibrary.Exposed.Models
{
    public class UserRoleModel
    {
        public string Name { get; internal set; }
        public Guid UserId { get; internal set; }
    }
}