﻿using System;

namespace AccountAssignUsersRolesLibrary.Exposed.Models
{
    public class UserRoleDeleteModel
    {
        public Guid UserId { internal get; set; }
        public Guid RoleId { internal get; set; }
    }
}