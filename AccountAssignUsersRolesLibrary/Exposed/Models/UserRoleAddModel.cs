﻿using System;

namespace AccountAssignUsersRolesLibrary.Exposed.Models
{
    public class UserRoleAddModel
    {
        public Guid UserId { internal get;  set; }
        public Guid RoleId { internal get; set; }
    }
}