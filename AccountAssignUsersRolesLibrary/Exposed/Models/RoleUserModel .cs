﻿using System;

namespace AccountAssignUsersRolesLibrary.Exposed.Models
{
    public class RoleUserModel
    {
        public string Name { get; internal set; }
        public Guid RoleId { get; internal set; }
    }
}