﻿using System;
using System.Collections.Generic;
using AccountAssignUsersRolesLibrary.Encapsulated.Factories;
using AccountAssignUsersRolesLibrary.Encapsulated.Managers;
using AccountAssignUsersRolesLibrary.Encapsulated.Repositories;
using AccountAssignUsersRolesLibrary.Exposed.Models;
using PostSharp.Patterns.Contracts;

namespace AccountAssignUsersRolesLibrary.Exposed.Publishers
{
    public class UsersToRolePublisher
    {
        readonly UsersRolesRepository _roleRepository = new UsersRolesRepository();
        readonly UserRoleManager _roleManager = new UserRoleManager();

        public List<UserRoleModel> Get(Guid value)
        {
            return UserRoleConvertFactory.Convert(this._roleManager.GetUsersByRole(value));
        }

        public void Add([NotNull] UserRoleAddModel model)
        {
            this._roleRepository.Add(UserRoleConvertFactory.ConvertToUser(model.UserId), UserRoleConvertFactory.ConvertRole(model.UserId));
        }

        public void Delete([NotNull] UserRoleDeleteModel model)
        {
            this._roleRepository.Add(UserRoleConvertFactory.ConvertToUser(model.UserId), UserRoleConvertFactory.ConvertRole(model.UserId));
        }
    }
}
