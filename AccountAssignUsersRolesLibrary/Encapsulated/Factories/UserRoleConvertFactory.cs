﻿using System;
using System.Collections.Generic;
using System.Linq;
using AccountAssignUsersRolesLibrary.Exposed.Models;
using DatabaseLibrary;

namespace AccountAssignUsersRolesLibrary.Encapsulated.Factories
{
    internal static class UserRoleConvertFactory
    {
        internal static AspNetUsers ConvertToUser(Guid userId)
        {
            return new AspNetUsers
            {
                Id = userId.ToString()
            };
        }

        internal static AspNetRoles ConvertRole(Guid roleId)
        {
            return new AspNetRoles
            {
                Id = roleId.ToString()
            };
        }

        internal static List<RoleUserModel> Convert(List<AspNetRoles> list)
        {
            return (list.Select(item => new RoleUserModel
            {
                 RoleId= Guid.Parse(item.Id),
                Name = item.Name
            })).ToList();
        }

        internal static List<UserRoleModel> Convert(List<AspNetUsers> list)
        {
            return (list.Select(item => new UserRoleModel
            {
                UserId = Guid.Parse(item.Id),
                Name = item.UserName
            })).ToList();
        }
    }
}
