﻿using System;
using System.Collections.Generic;
using System.Linq;
using AccountAssignUsersRolesLibrary.Encapsulated.Factories;
using AccountAssignUsersRolesLibrary.Encapsulated.Repositories;
using AccountAssignUsersRolesLibrary.Exposed.Models;
using DatabaseLibrary;

namespace AccountAssignUsersRolesLibrary.Encapsulated.Managers
{
    internal class UserRoleManager
    {
        readonly UsersRolesRepository _roleRepository = new UsersRolesRepository();

        internal List<AspNetRoles> GetRolesByUser(Guid value)
        {
            return _roleRepository.GetRolesAssigenToUser(value);
        }

        internal List<AspNetUsers> GetUsersByRole(Guid value)
        {
            return _roleRepository.GetUsersAssigenToRole(value);
        }
    }
}
