﻿using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseLibrary;
using PostSharp.Patterns.Contracts;

namespace AccountAssignUsersRolesLibrary.Encapsulated.Repositories
{
    internal class UsersRolesRepository
    {
        readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();

        internal List<AspNetRoles> GetRolesAssigenToUser(Guid value)
        {
            return this._copenhagenByNightEntities.AspNetUsers.Where(item => item.Id == value.ToString())
                    .SelectMany(role => role.AspNetRoles)
                    .ToList();
        }

        internal List<AspNetUsers> GetUsersAssigenToRole( Guid value)
        {
            return this._copenhagenByNightEntities.AspNetRoles.Where(item => item.Id == value.ToString())
                    .SelectMany(role => role.AspNetUsers)
                    .ToList();
        }

        internal void Add(AspNetUsers aspNetUsers1, AspNetUsers aspNetUsers2)
        {
            throw new NotImplementedException();
        }

        internal void Add([NotNull] AspNetUsers user, [NotNull] AspNetRoles role)
        {
            this._copenhagenByNightEntities.AspNetUsers.Attach(user);
            this._copenhagenByNightEntities.AspNetRoles.Attach(role);
            role.AspNetUsers.Add(user);
            this._copenhagenByNightEntities.SaveChanges();
        }

        internal void Delete([NotNull] AspNetUsers user, [NotNull] AspNetRoles role)
        {
            this._copenhagenByNightEntities.AspNetUsers.Attach(user);
            this._copenhagenByNightEntities.AspNetRoles.Attach(role);
            role.AspNetUsers.Remove(user);
            this._copenhagenByNightEntities.SaveChanges();
        }
    }
}
