﻿namespace HumanityLibrary
{
    using System;
    using System.Collections.Generic;
    using Exposed.Models;
    using PostSharp.Patterns.Contracts;

    public interface IHumanityPublisher
    {
        HumanityModel Add([NotNull] HumanityAddModel model);
        bool Delete(Guid humanityId);
        List<HumanityModel> Get();
        HumanityModel Get(Guid humanityId);
        HumanityModel Update(Guid humanityId, [NotNull] HumanityUpdateModel model);
    }
}