﻿using System;
using PostSharp.Patterns.Contracts;

namespace HumanityLibrary.Exposed.Models
{
    public class HumanityUpdateModel
    {
        public string Comments { get; internal set; }
        public string Description { get; internal set; }
        public Guid HumanityId { get; internal set; }
        public int HumanityLevel { get; internal set; }
        public int HuntingBonus { get; internal set; }
        public int LengthOfTorpor { get; internal set; }
        public string MoralStandards { get; internal set; }
        public int NumberOfTouchstones { get; internal set; }
        public string Sins { get; internal set; }
    }
}