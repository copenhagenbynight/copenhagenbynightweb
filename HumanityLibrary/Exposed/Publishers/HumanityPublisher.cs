﻿namespace HumanityLibrary.Exposed.Publishers
{
    using System;
    using System.Collections.Generic;
    using Encapsulated.Managers;
    using Models;
    using PostSharp.Patterns.Contracts;

    public class HumanityPublisher : IHumanityPublisher
    {
        private readonly HumanityManager _humanityManager = new HumanityManager();

        public List<HumanityModel> Get()
        {
            return this._humanityManager.Get();
        }

        public HumanityModel Get(Guid humanityId)
        {
            return this._humanityManager.Get(humanityId);
        }

        public HumanityModel Add([NotNull] HumanityAddModel model)
        {
            return this._humanityManager.Add(model);
        }

        public HumanityModel Update(Guid humanityId, [NotNull] HumanityUpdateModel model)
        {
            return this._humanityManager.Update(humanityId, model);
        }

        public bool Delete(Guid humanityId)
        {
            return this._humanityManager.Delete(humanityId);
        }
    }
}
