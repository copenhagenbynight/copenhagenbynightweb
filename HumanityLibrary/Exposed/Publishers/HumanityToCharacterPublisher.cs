﻿namespace HumanityLibrary.Exposed.Publishers
{
    using System;
    using Encapsulated.Managers;
    using Models;

    public class HumanityToCharacterPublisher : IHumanityToCharacterPublisher
    {
    private readonly Guid _characterId;
    private readonly HumanityManager _humanityManager = new HumanityManager();
    private readonly HumanityToCharacterManager _humanityToCharacterManager = new HumanityToCharacterManager();

    public HumanityToCharacterPublisher(Guid characterId)
    {
        this._characterId = characterId;
    }

    public HumanityModel Set(Guid humanityId)
    {
        this._humanityToCharacterManager.Update(this._characterId, humanityId);
        return this._humanityManager.Get(humanityId);
    }

    public void SetDefault()
    {
        this._humanityToCharacterManager.SetDefault(this._characterId);
    }

    public HumanityModel Get()
    {
        return this._humanityToCharacterManager.Get(this._characterId);


    }
}
}
