﻿namespace HumanityLibrary
{
    using System;
    using Exposed.Models;

    public interface IHumanityToCharacterPublisher
    {
        HumanityModel Get();
        HumanityModel Set(Guid humanityId);
        void SetDefault();
    }
}