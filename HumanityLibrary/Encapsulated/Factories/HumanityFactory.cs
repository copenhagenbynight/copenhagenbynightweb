﻿namespace HumanityLibrary.Encapsulated.Factories
{
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseLibrary;
    using Exposed.Models;
    using PostSharp.Patterns.Contracts;

    internal class HumanityFactory
    {
        internal HumanityModel Convert([NotNull] Humanity humanity)
        {
            return new HumanityModel
            {
                HumanityId = humanity.HumanityId,
                HumanityLevel = humanity.HumanityLevel,
                Description = humanity.Description,
                Sins = humanity.Sins,
                NumberOfTouchstones = humanity.NumberOfTouchstones,
                HuntingBonus = humanity.HuntingBonus,
                Comments = humanity.Comments,
                LengthOfTorpor = humanity.LengthOfTorpor,
                MoralStandards = humanity.MoralStandards
            };
        }

        internal Humanity Convert([NotNull] HumanityAddModel humanity)
        {
            return new Humanity
            {
                HumanityLevel = humanity.HumanityLevel,
                Description = humanity.Description,
                Sins = humanity.Sins,
                NumberOfTouchstones = humanity.NumberOfTouchstones,
                HuntingBonus = humanity.HuntingBonus,
                Comments = humanity.Comments,
                LengthOfTorpor = humanity.LengthOfTorpor,
                MoralStandards = humanity.MoralStandards
            };
        }

        internal Humanity Convert([NotNull] HumanityUpdateModel humanity)
        {
            return new Humanity
            {
                HumanityId = humanity.HumanityId,
                HumanityLevel = humanity.HumanityLevel,
                Description = humanity.Description,
                Sins = humanity.Sins,
                NumberOfTouchstones = humanity.NumberOfTouchstones,
                HuntingBonus = humanity.HuntingBonus,
                Comments = humanity.Comments,
                LengthOfTorpor = humanity.LengthOfTorpor,
                MoralStandards = humanity.MoralStandards
            };
        }

        internal List<HumanityModel> Convert([NotNull] List<Humanity> list)
        {
            return (from item in list select this.Convert(item)).ToList();
        }
    }
}
