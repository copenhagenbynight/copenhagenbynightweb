﻿namespace HumanityLibrary.Encapsulated.Repositories
{
    using System;
    using System.Linq;
    using AspectLibrary;
    using DatabaseLibrary;

    internal class HumanityToCharacterRepository
    {
        private readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();

        internal Humanity Get([GuidNotNull] Guid characterId)
        {
            return
                this._copenhagenByNightEntities.Character.Where(item => item.CharacterId == characterId)
                    .Select(item => item.Humanity).FirstOrDefault();
        }

        internal Humanity Update([GuidNotNull] Guid characterId, [GuidNotNull] Guid humanityId)
        {
            Character currentCharacter =
                this._copenhagenByNightEntities.Character.FirstOrDefault(item => item.CharacterId == characterId);

            // ReSharper disable once PossibleNullReferenceException
            currentCharacter.HumanityId = humanityId;

            this._copenhagenByNightEntities.SaveChanges();

            return currentCharacter.Humanity;
        }
    }
}
