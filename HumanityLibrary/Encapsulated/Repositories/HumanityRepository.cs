﻿namespace HumanityLibrary.Encapsulated.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseLibrary;

    internal class HumanityRepository
    {
        private readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();

        internal List<Humanity> Get()
        {
            return this._copenhagenByNightEntities.Humanity.OrderBy(item => item.HumanityLevel).Select(item => item).ToList();
        }

        internal Humanity Get(Guid humanityId)
        {
            return this._copenhagenByNightEntities.Humanity.FirstOrDefault(item => item.HumanityId == humanityId);
        }

        internal Humanity Add(Humanity value)
        {
            this._copenhagenByNightEntities.Humanity.Add(value);
            this._copenhagenByNightEntities.SaveChanges();
            return value;
        }

        internal Humanity Update(Guid humanityId, Humanity value)
        {
            Humanity currentHumanity = new Humanity { HumanityId = humanityId };
            this._copenhagenByNightEntities.Humanity.Attach(currentHumanity);

           
            currentHumanity.HumanityLevel = value.HumanityLevel;
            currentHumanity.Description = value.Description;
            currentHumanity.Sins = value.Sins;
            currentHumanity.NumberOfTouchstones = value.NumberOfTouchstones;
            currentHumanity.HuntingBonus = value.HuntingBonus;
            currentHumanity.Comments = value.Comments;          
            currentHumanity.LengthOfTorpor = value.LengthOfTorpor;
            currentHumanity.MoralStandards = value.MoralStandards;




            this._copenhagenByNightEntities.SaveChanges();
            return value;
        }

        internal bool Delete(Guid humanityId)
        {
            Humanity currentHumanity = new Humanity {HumanityId = humanityId};
            this._copenhagenByNightEntities.Humanity.Attach(currentHumanity);
            this._copenhagenByNightEntities.Humanity.Remove(currentHumanity);
            this._copenhagenByNightEntities.SaveChanges();
            return true;
        }
    }
}
