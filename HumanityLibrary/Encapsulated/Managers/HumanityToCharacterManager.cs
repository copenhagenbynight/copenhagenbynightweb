﻿namespace HumanityLibrary.Encapsulated.Managers
{
    using System;
    using System.Linq;
    using AspectLibrary;
    using Exposed.Models;
    using Factories;
    using Repositories;

    internal class HumanityToCharacterManager
    {
        private readonly HumanityRepository _humanityRepository = new HumanityRepository();

        private readonly HumanityToCharacterRepository _humanityToCharacterRepository =
            new HumanityToCharacterRepository();

        private readonly HumanityFactory _humanityFactory = new HumanityFactory();

        internal HumanityModel Update([GuidNotNull] Guid characterId, [GuidNotNull] Guid humanityId)
        {
            return
                this._humanityFactory.Convert(this._humanityToCharacterRepository.Update(characterId,
                    humanityId));
        }

        internal HumanityModel SetDefault([GuidNotNull] Guid characterId)
        {
            // ReSharper disable once PossibleNullReferenceException
            return this.Update(characterId, this._humanityRepository.Get().FirstOrDefault().HumanityId);
        }

        internal HumanityModel Get([GuidNotNull] Guid characterId)
        {
            return this._humanityFactory.Convert(this._humanityToCharacterRepository.Get(characterId));
        }
    }
}
