﻿namespace HumanityLibrary.Encapsulated.Managers
{
    using System;
    using System.Collections.Generic;
    using Exposed.Models;
    using Factories;
    using Repositories;

    internal class HumanityManager
    {
        private readonly HumanityRepository _humanityRepository = new HumanityRepository();
        private readonly HumanityFactory _humanityConvertFactory = new HumanityFactory();

        internal List<HumanityModel> Get()
        {
            return this._humanityConvertFactory.Convert(this._humanityRepository.Get());
        }

        internal HumanityModel Get(Guid humanityId)
        {
            return this._humanityConvertFactory.Convert(this._humanityRepository.Get(humanityId));
        }

        internal HumanityModel Add(HumanityAddModel model)
        {
            return this._humanityConvertFactory.Convert(this._humanityRepository.Add(this._humanityConvertFactory.Convert(model)));
        }

        internal HumanityModel Update(Guid humanityId, HumanityUpdateModel model)
        {
            return
                this._humanityConvertFactory.Convert(this._humanityRepository.Update(humanityId,
                    this._humanityConvertFactory.Convert(model)));
        }

        internal bool Delete(Guid value)
        {
            return this._humanityRepository.Delete(value);
        }
    }
}
