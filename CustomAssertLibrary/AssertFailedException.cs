﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomAssertLibrary
{
    using System.Runtime.Serialization;

    [Serializable]
    internal class AssertFailedException : Exception
    {
        public AssertFailedException()
        {
        }

        public AssertFailedException(string message) : base(message)
        {
        }

        public AssertFailedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AssertFailedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
