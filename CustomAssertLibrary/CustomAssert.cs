﻿namespace CustomAssertLibrary
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    public class CustomAssert
    {
        public static void Throws<T>(Action func) where T : Exception
        {
            bool exceptionThrown = false;
            try
            {
                func.Invoke();
            }
            catch (T)
            {
                exceptionThrown = true;
            }

            if (!exceptionThrown)
            {
                throw new AssertFailedException(
                    $"An exception of type {typeof(T)} was expected, but not thrown"
                );
            }
        }

        public static void PropertyIsCorrect(Type type, Dictionary<string, Type> listOfTypeValidations)
        {
            int index = 0;
            foreach (PropertyInfo propertyInfo in type.GetProperties())
            {
                if (!listOfTypeValidations.ContainsKey(propertyInfo.Name))
                {
                    throw new AssertFailedException(
                        $"An exception of property name {propertyInfo.Name} was but not expected"
                    );
                }

                Type typeValidation = listOfTypeValidations[propertyInfo.Name];

                if (typeValidation != propertyInfo.PropertyType)
                {
                    throw new AssertFailedException(
                        $"An exception of property type expected {typeValidation} found {propertyInfo.PropertyType}."
                    );
                }

                index++;
            }


            if (listOfTypeValidations.Count != index)
            {
                throw new AssertFailedException(
                    $"An wrong number of property found expected {listOfTypeValidations.Count} found {index}.");
            }
        }
    }
}
