﻿namespace ClanLibrary.Encapsulated.Factories
{
    using System.Collections.Generic;
    using System.Linq;
    using DatabaseLibrary;
    using Exposed.Models;
    using PostSharp.Patterns.Contracts;

    internal class ClanFactory
    {
        internal ClanModel Convert([NotNull] Clan clan)
        {
            return new ClanModel
            {
                Name = clan.Name,
                ClanId = clan.ClanId,
                Description = clan.Description
            };
        }

        internal Clan Convert([NotNull] ClanAddModel clan)
        {
            return new Clan
            {
                Name = clan.Name,
                Description = clan.Description
            };
        }

        internal Clan Convert([NotNull] ClanUpdateModel clan)
        {
            return new Clan
            {
                ClanId = clan.ClanId,
                Name = clan.Name,
                Description = clan.Description
            };
        }

        internal List<ClanModel> Convert([NotNull] List<Clan> list)
        {
            return (list.Select(this.Convert)).ToList();
        }
    }
}
