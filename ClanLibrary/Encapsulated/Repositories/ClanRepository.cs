﻿namespace ClanLibrary.Encapsulated.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AspectLibrary;
    using DatabaseLibrary;
    using PostSharp.Patterns.Contracts;

   
    internal class ClanRepository
    {
        private readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();

        [CustomLogAspect]
        [TranslateScopeAspect(MaxRetries = 3)]
        internal List<Clan> Get()
        {
            return this._copenhagenByNightEntities.Clan.OrderBy(item => item.Name).Select(item => item).OrderBy(item => item.Name).ToList();
        }

        [TranslateScopeAspect(MaxRetries = 3)]
        internal Clan Get([GuidNotNull] Guid clanId)
        {
            return this._copenhagenByNightEntities.Clan.FirstOrDefault(item => item.ClanId == clanId);
        }

        [TranslateScopeAspect(MaxRetries = 3)]
        internal Clan Add([NotNull] Clan value)
        {

           value.ClanId = Guid.NewGuid();
            this._copenhagenByNightEntities.Clan.Add(value);
            this._copenhagenByNightEntities.SaveChanges();
            return value;
        }

        [TranslateScopeAspect(MaxRetries = 3)]
        internal Clan Update([GuidNotNull]  Guid clanId,[NotNull] Clan value)
        {
            Clan currentClan = new Clan { ClanId = clanId };
            this._copenhagenByNightEntities.Clan.Attach(currentClan);

            currentClan.Description = value.Description;
            currentClan.Name = value.Name;
               
            this._copenhagenByNightEntities.SaveChanges();
            return value;
        }

        [TranslateScopeAspect(MaxRetries = 3)]
        internal bool Delete([GuidNotNull] Guid clanId)
        {
            Clan currentClan = (this._copenhagenByNightEntities.Clan.FirstOrDefault(item => item.ClanId == clanId));
            this._copenhagenByNightEntities.Clan.Remove(currentClan);
            this._copenhagenByNightEntities.SaveChanges();
            return true;
        }
    }
}
