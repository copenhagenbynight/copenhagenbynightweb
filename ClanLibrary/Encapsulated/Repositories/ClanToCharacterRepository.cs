﻿namespace ClanLibrary.Encapsulated.Repositories
{
    using System;
    using System.Linq;
    using AspectLibrary;
    using DatabaseLibrary;

   
    internal class ClanToCharacterRepository
    {
        private readonly CopenhagenByNightEntities _copenhagenByNightEntities = new CopenhagenByNightEntities();

        [TranslateScopeAspect(MaxRetries = 3)]
        internal Character SetClan([GuidNotNull] Guid characterId, [GuidNotNull] Guid clanId)
        {
            Character currentCharacter =
                this._copenhagenByNightEntities.Character.FirstOrDefault(item => item.CharacterId == characterId);

            // ReSharper disable once PossibleNullReferenceException
            currentCharacter.ClanId = clanId;

            this._copenhagenByNightEntities.SaveChanges();

            return currentCharacter;
        }

        [TranslateScopeAspect(MaxRetries = 3)]
        internal Clan GetClan([GuidNotNull] Guid characterId)
        {
            return this._copenhagenByNightEntities.Character.Where(item => item.CharacterId == characterId)
                .Select(item => item.Clan).FirstOrDefault();
        }
    }
}
