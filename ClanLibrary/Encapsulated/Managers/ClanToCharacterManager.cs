﻿using System;
using System.Linq;
using ClanLibrary.Exposed.Models;
using ClanLibrary.Encapsulated.Repositories;

namespace ClanLibrary.Encapsulated.Managers
{
    using AspectLibrary;
    using Factories;

    internal class ClanToCharacterManager
    {
        private readonly ClanRepository _clanRepository = new ClanRepository();
        private readonly ClanToCharacterRepository _clanToCharacterRepository = new ClanToCharacterRepository();
        private readonly ClanFactory _clanConvertFactory = new ClanFactory();
        internal void SetClan([GuidNotNull]  Guid characterId, [GuidNotNull]  Guid clanId)
        {
            this._clanToCharacterRepository.SetClan(characterId, clanId);
        }

        internal ClanModel GetClan([GuidNotNull]  Guid characterId)
        {
            return this._clanConvertFactory.Convert(this._clanToCharacterRepository.GetClan(characterId));
        }

        public void SetDefault([GuidNotNull] Guid characterId)
        {
            // ReSharper disable once PossibleNullReferenceException
            this.SetClan(characterId, this._clanRepository.Get().FirstOrDefault().ClanId);          
        }
    }
}
