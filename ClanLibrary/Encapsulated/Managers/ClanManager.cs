﻿namespace ClanLibrary.Encapsulated.Managers
{
    using System;
    using System.Collections.Generic;
    using AspectLibrary;
    using Exposed.Models;
    using Factories;
    using PostSharp.Patterns.Contracts;
    using Repositories;

    internal class ClanManager
    {
        private readonly ClanRepository _clanRepository = new ClanRepository();
        private readonly ClanFactory _clanConvertFactory = new ClanFactory();

        internal List<ClanModel> Get()
        {
            return this._clanConvertFactory.Convert(this._clanRepository.Get());
        }

        internal ClanModel Get([GuidNotNull] Guid clanId)
        {
            return this._clanConvertFactory.Convert(this._clanRepository.Get(clanId));
        }

        internal ClanModel Add([NotNull] ClanAddModel model)
        {
            return this._clanConvertFactory.Convert(this._clanRepository.Add(this._clanConvertFactory.Convert(model)));
        }

        internal ClanModel Update([GuidNotNull] Guid clanId,[NotNull] ClanUpdateModel model)
        {
            return
                this._clanConvertFactory.Convert(this._clanRepository.Update(clanId,
                    this._clanConvertFactory.Convert(model)));
        }

        internal bool Delete([GuidNotNull] Guid value)
        {
            return this._clanRepository.Delete(value);
        }
    }
}
