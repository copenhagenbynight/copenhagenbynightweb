﻿namespace ClanLibrary
{
    using System;
    using System.Collections.Generic;
    using Exposed.Models;
    using PostSharp.Patterns.Contracts;
    using AspectLibrary;

    public interface IClanPublisher
    {
        ClanModel Add([NotNull] ClanAddModel model);
        bool Delete([GuidNotNull] Guid clanId);
        List<ClanModel> Get();
        ClanModel Get([GuidNotNull] Guid clanId);
        ClanModel Update([GuidNotNull] Guid clanId, [NotNull] ClanUpdateModel model);
    }
}