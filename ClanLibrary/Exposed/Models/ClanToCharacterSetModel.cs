﻿namespace ClanLibrary.Exposed.Models
{
    using System;
    using AspectLibrary;

    public class ClanToCharacterSetModel
    {
        [GuidNotNull]
        public Guid ClanId { get; set; }
    }
}