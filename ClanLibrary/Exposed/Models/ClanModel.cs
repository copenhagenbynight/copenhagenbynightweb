﻿using System;
using PostSharp.Patterns.Contracts;

namespace ClanLibrary.Exposed.Models
{
    using AspectLibrary;

    public class ClanModel : ClanBaseModel
    {
        [GuidNotNull]
        public Guid ClanId { get; set; }
    }
}