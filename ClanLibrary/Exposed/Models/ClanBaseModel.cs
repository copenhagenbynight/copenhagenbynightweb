﻿using System;
using PostSharp.Patterns.Contracts;

namespace ClanLibrary.Exposed.Models
{
    using AspectLibrary;

    public class ClanBaseModel
    {
        [StringLength(1, 239)]
        public string Name {  get; set; }
        [StringLength(1, 9999)]
        public string Description {  get; set; }

    }
}