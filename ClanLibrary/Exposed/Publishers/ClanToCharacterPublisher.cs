﻿namespace ClanLibrary.Exposed.Publishers
{
    using System;
    using AspectLibrary;
    using Encapsulated.Managers;
    using Models;

   
    public class ClanToCharacterPublisher : IClanToCharacterPublisher
    {
        private readonly Guid _characterId;
        private readonly ClanManager _clanManager = new ClanManager();
        private readonly ClanToCharacterManager _clanToCharacterManager = new ClanToCharacterManager();

        public ClanToCharacterPublisher([GuidNotNull] Guid characterId)
        {
            this._characterId = characterId;
        }

        public ClanModel Update([GuidNotNull] Guid clanId)
        {
            return this.Update(this._characterId, clanId);

        }

        public void SetDefault()
        {
            this.SetDefault(this._characterId);
        }

        public ClanModel Get()
        {
            return this.Get(this._characterId);
           
        }

        [CacheAspect]
        private ClanModel Update([GuidNotNull] Guid characterId,[GuidNotNull] Guid clanId)
        {
            this._clanToCharacterManager.SetClan(characterId, clanId);
            return this._clanManager.Get(clanId);
        }

        [CacheAspect]
        private void SetDefault([GuidNotNull] Guid characterId)
        {
            this._clanToCharacterManager.SetDefault(characterId);
        }

        [CacheAspect]
        private ClanModel Get([GuidNotNull] Guid characterId)
        {
            return this._clanToCharacterManager.GetClan(characterId);

        }
    }
}
