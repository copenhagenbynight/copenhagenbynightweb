﻿namespace ClanLibrary.Exposed.Publishers
{
    using System;
    using System.Collections.Generic;
    using AspectLibrary;
    using Encapsulated.Managers;
    using Models;
    using PostSharp.Patterns.Contracts;

    
    public class ClanPublisher : IClanPublisher
    {
        private readonly ClanManager _clanManager = new ClanManager();

        [CustomLogAspect]
      //  [CacheAspect]
        public List<ClanModel> Get()
        {
            return this._clanManager.Get();
        }

        [CacheAspect]
        public ClanModel Get([GuidNotNull] Guid clanId)
        {
            return this._clanManager.Get(clanId);
        }

        [CacheAspect]
        public ClanModel Add([NotNull] ClanAddModel model)
        {
            return this._clanManager.Add(model);
        }

        [CacheAspect]
        public ClanModel Update([GuidNotNull] Guid clanId, [NotNull] ClanUpdateModel model)
        {
            return this._clanManager.Update(clanId, model);
        }

        [CacheAspect]
        public bool Delete([GuidNotNull] Guid clanId)
        {
           return this._clanManager.Delete(clanId);
        }
    }
}
