﻿namespace ClanLibrary
{
    using System;
    using AspectLibrary;
    using Exposed.Models;

    public interface IClanToCharacterPublisher
    {
        ClanModel Get();
        ClanModel Update([GuidNotNull] Guid clanId);
        void SetDefault();
    }
}