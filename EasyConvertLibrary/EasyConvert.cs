﻿namespace EasyConvertLibrary
{
    using System.Collections.Generic;
    using System.Linq;
    using Nelibur.ObjectMapper;

    public static class EasyConvert
    {
        public static T Convert<TF, T>(TF item)
        {
            TinyMapper.Bind<TF, T>();

            return TinyMapper.Map<T>(item);
        }

        public static List<T> Convert<TF, T>(List<TF> items)
        {
            return Enumerable.ToList(items.Select(Convert<TF, T>));
        }
    }
}